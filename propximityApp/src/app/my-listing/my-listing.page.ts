import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { UtilitiesService } from '../services/utilities.service';
import { Router } from '@angular/router';
import { AccountService } from '../services/account.service';
import { PropertyService } from '../services/property.service';
import { HttpService } from '../services/http.service';
import * as moment from 'moment';
@Component({
  selector: 'app-my-listing',
  templateUrl: './my-listing.page.html',
  styleUrls: ['./my-listing.page.scss'],
})
export class MyListingPage implements OnInit {

  isTab: boolean = true;
  tabState: any = 'draft';

  showModal: boolean = false;
  publishCredit: any = 0;
  draftListing: any = [];
  publishListing: any = [];
  selectedListing: any = [];
  tagAgents: any = '';

  formtag = {
    name: '',
    number: ''
  }


  constructor(private location: Location,
    private _utilitiesService: UtilitiesService,
    private router: Router,
    private _accountService: AccountService,
    private _propertyService: PropertyService,
    private _httpService: HttpService) { }

  ngOnInit() {

  }

  goBack(){
    this.location.back();
  }

  ionViewWillEnter() {
    this._accountService.checkUserSession();
    this.myListing();
    let params = {
      account_id: this._accountService.userData.id
    };
    this._propertyService.getTagAgents(params, ()=>{

    });
    this._accountService.getUserDetail(()=>{
      let config = JSON.parse(localStorage.getItem('config'));
      this.publishCredit = config[4].val;
    });

  }

  addTag(){
    let params = {
      account_id: this._accountService.userData.id,
      name: this.formtag.name,
      mobile: this.formtag.number
    }
    this._propertyService.createTagAgents(params, ()=>{
      this._propertyService.getTagAgents(params, ()=>{});
    });
  }

  getTagAgents(){
    return this._propertyService.tagAgents;
  }

  async publishAlert(item) {
    const forgot = await this._utilitiesService.getAlertCtrl().create({
      header: 'Publish',
      message: "Publish Property with active viewing time slot. 1 FOR "+this.publishCredit+" CREDIT ",
      
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Publish',
          handler: data => {
            console.log('Send clicked');
            let param = {
              id: item.id,
              credit: this.publishCredit,
              agents: this.tagAgents
            }
            this._accountService.publish(param, () => {
              this._utilitiesService.alertMessageCallback('Success','Listing published successfully !', ()=>{
                this.showModal = false;
                this.myListing();
              });
            });
            // let toast = this.toastCtrl.create({
            //   message: 'Email was sended successfully',
            //   duration: 3000,
            //   position: 'top',
            //   cssClass: 'dark-trans',
            //   closeButtonText: 'OK',
            //   showCloseButton: true
            // });
            // toast.present();
          }
        }
      ]
    });
    forgot.present();
  }

  publish(){
    console.log(this._propertyService.tagAgents);
    this.tagAgents =  '';
    for(var i=0;i<this._propertyService.tagAgents.length;i++){
      if(this._propertyService.tagAgents[i].checked == true){
        this.tagAgents += this._propertyService.tagAgents[i].id+',';
      }
    }
    if(this.tagAgents !== ''){
      this.tagAgents = this.tagAgents.substring(0, this.tagAgents.length - 1);
    }
    this.publishAlert(this.selectedListing);

  }

  toggleTab(string) {
    this.tabState = string;
  }

  getTopupHistory(){
    return this._accountService.topupHistory;
  }

  openBrowser(title, url){
    this._utilitiesService.openBrowser(title, url);
  }

  myListing() {
    this.draftListing = [];
    this.publishListing = [];
    let params = {
      account_id: this._accountService.userData.id
    }
    this._propertyService.getMyListing(params, () => {
      for(var i=0;i<this._propertyService.myListing.length;i++){
        if(this._propertyService.myListing[i].status == '1'){
          this.publishListing.push(this._propertyService.myListing[i]);
        }else{
          this.draftListing.push(this._propertyService.myListing[i]);
        }
      }
      console.log(this.draftListing);
      console.log(this.publishListing);
      this._accountService.userData = this._propertyService.account_detail;
      
    });
  }

  tagAgent(item){
    this.selectedListing = item;
    this.showModal = true;
    // this.publishAlert(item);
  }

  formatDate(dt){
    return moment(dt, 'YYYY-MM-DD HH:mm:ss').format('DD MMM');
  }


  formatTime(dt){
    return moment(dt, 'YYYY-MM-DD HH:mm:ss').format('hh a');
  }

  edit(item){
    this._propertyService.selectedListing = item;
    let temp = item.available_date_time;
    this._propertyService.availableDateTimeViewing = [];
    for(var i=0;i<temp.length;i++){
      var startDate = moment(temp[i].available_date_from, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD');
      var startTime = moment(temp[i].available_date_from, 'YYYY-MM-DD HH:mm:ss').format('HH:mm');
      var endDate = moment(temp[i].available_date_to, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD');
      var endTime = moment(temp[i].available_date_to, 'YYYY-MM-DD HH:mm:ss').format('HH:mm');
      let data = {
        startDate: moment(temp[i].available_date_from, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD'),
        startDateTime: startDate+' '+startTime+':00',
        startTime: startTime,
        endDate: endDate,
        endDateTime: endDate+ ' '+endTime+':00',
        endTime: endTime,
        display: moment(startDate+' '+startTime, 'YYYY-MM-DD HH:mm').format('DD MMM YYYY HH:mm')+ ' to ' +moment(endDate+ ' '+endTime, 'YYYY-MM-DD HH:mm').format('DD MMM YYYY HH:mm')
      };
      this._propertyService.availableDateTimeViewing.push(data);
    }
    this.router.navigateByUrl('/edit-post');
  }

  getListing() {
    return this._propertyService.myListing;
  }

  getUserData(){
    return this._accountService.userData;
  }


}
