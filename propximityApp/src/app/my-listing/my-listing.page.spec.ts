import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyListingPage } from './my-listing.page';

describe('MyListingPage', () => {
  let component: MyListingPage;
  let fixture: ComponentFixture<MyListingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyListingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyListingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
