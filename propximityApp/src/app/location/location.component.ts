import { Component, OnInit } from '@angular/core';
import { PropertyService } from '../services/property.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss'],
})
export class LocationComponent implements OnInit {

  constructor(private _propertyService: PropertyService,
              private modalController: ModalController) { }

  selected: any = '';
  ngOnInit() {}

  close(){
    this.modalController.dismiss({'value': this.selected});
  }

  getDistrict(){
    return this._propertyService.districts;
  }

  getHdbStates(){
    return this._propertyService.hdbStates;
  }

  select(item){
    this.modalController.dismiss({'key': item.key, 'value': item.value});
  }

  getSelectedPropertyType(){
    return this._propertyService.selectedPropertyType;
  }

}
