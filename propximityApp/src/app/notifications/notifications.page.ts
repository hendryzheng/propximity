import { Component, OnInit } from '@angular/core';
import { AccountService } from '../services/account.service';
import { PropertyService } from '../services/property.service';
import { UtilitiesService } from '../services/utilities.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {

  tabState = 'viewing-request';

  constructor(private _accountService: AccountService,
              private router: Router,
              private _utilitiesService: UtilitiesService,
              private _propertyService: PropertyService) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this._accountService.checkUserSession();
    var params = {};
    if(this._accountService.isUserSessionAlive()){
      params = {
        'account_id' : this._accountService.userData.id
      }
    }
    
    this._accountService.getNotifications(params, ()=>{

    });
  }

  getNotificationViewingRequest(){
    return this._accountService.notificationViewingRequest;
  }

  openTagAgents(item){
    if(item.payload !== null){
      let json = JSON.parse(item.payload);
      console.log(json);

      this._propertyService.selectedListing = json;
      this.router.navigateByUrl('/send-request');
    }
  }

  getNotificationTagAgents(){
    return this._accountService.notificationTagAgents;
  }

  open(item){
    this._utilitiesService.alertMessage(item.title,item.notification);
  }

  toggleTab(string) {
    this.tabState = string;
  }

}
