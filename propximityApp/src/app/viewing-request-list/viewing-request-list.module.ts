import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ViewingRequestListPage } from './viewing-request-list.page';

const routes: Routes = [
  {
    path: '',
    component: ViewingRequestListPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ViewingRequestListPage]
})
export class ViewingRequestListPageModule {}
