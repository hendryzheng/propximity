import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewingRequestListPage } from './viewing-request-list.page';

describe('ViewingRequestListPage', () => {
  let component: ViewingRequestListPage;
  let fixture: ComponentFixture<ViewingRequestListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewingRequestListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewingRequestListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
