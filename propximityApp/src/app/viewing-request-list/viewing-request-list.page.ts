import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { UtilitiesService } from '../services/utilities.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { AccountService } from '../services/account.service';

import * as moment from 'moment';

declare var cordova: any;

@Component({
  selector: 'app-viewing-request-list',
  templateUrl: './viewing-request-list.page.html',
  styleUrls: ['./viewing-request-list.page.scss'],
})
export class ViewingRequestListPage implements OnInit {

  listing: any = [];

  constructor(private location: Location,
    private _utilitiesService: UtilitiesService,
    private _accountService: AccountService,
    private _httpService: HttpService) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.fetch();
  }
  

  goBack() {
    this.location.back();
  }

  fetch() {
    this._utilitiesService.showLoading();
    let params = {
      account_id : this._accountService.userData.id
    }
    this._httpService.postRequest(CONFIGURATION.URL.getViewingRequest, params).subscribe(response => {

      this._utilitiesService.hideLoading().then(resp => {
        if (response.status === 'OK') {
          this.listing = response.data.listing;
        } else {
          this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
        }
      });

    }, error => {
      this._utilitiesService.hideLoading();
      console.log(error);
    });
  }


  openBrowser(title, url){
    this._utilitiesService.openBrowser(title, url);
  }

  formatDate(dt){
    return moment(dt, 'YYYY-MM-DD HH:mm:ss').format('DD MMM');
  }

  whatsapp(phone){
    var url =  'https://api.whatsapp.com/send?phone='+this._utilitiesService.formatWhatsappNo(phone);
    var ref = cordova.InAppBrowser.open(url, '_system', 'location=yes');

  }

  formatTime(dt){
    return moment(dt, 'YYYY-MM-DD HH:mm:ss').format('hh a');
  }

}
