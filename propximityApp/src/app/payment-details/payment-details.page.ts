import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

declare var Stripe;
import * as moment from 'moment';
import { PropertyService } from '../services/property.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-payment-details',
  templateUrl: './payment-details.page.html',
  styleUrls: ['./payment-details.page.scss'],
})
export class PaymentDetailsPage implements OnInit {

  form = {
    account_purchase_id: 0,
    card_name: '',
    token: '',
    payment_date: moment().format('YYYY-MM-DD HH:mm:ss'),
    payment_status:0
  };

  constructor(private location: Location,
              private router: Router,
              private _propertyService: PropertyService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.form.account_purchase_id = this._propertyService.selectedAccountTopupPurchase.id;
    this.form.payment_status = 0;
    this.setupStripe();
  }

  getSelectedPackage(){
    return this._propertyService.selectedPackage;
  }

  setupStripe(){
    // Create a Stripe client.
    var stripe = Stripe('pk_live_ZVJkb9ui8QSn8rfoT35UYnOT00miePcr3Y');

    // Create an instance of Elements.
    var elements = stripe.elements();

    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
      base: {
        color: '#32325d',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#aab7c4'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    };

    // Create an instance of the card Element.
    var card = elements.create('card', { style: style });

    // Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');

    // Handle real-time validation errors from the card Element.
    card.addEventListener('change', event => {
      var displayError = document.getElementById('card-errors');
      if (event.error) {
        displayError.textContent = event.error.message;
      } else {
        displayError.textContent = '';
      }
    });

    // Handle form submission.
    var form = document.getElementById('payment-form');
    form.addEventListener('submit', event => {
      event.preventDefault();

      stripe.createToken(card).then( result => {
        if (result.error) {
          // Inform the user if there was an error.
          var errorElement = document.getElementById('card-errors');
          errorElement.textContent = result.error.message;
        } else {
          // Send the token to your server.
          console.log(result);
          this.stripeTokenHandler(result.token);
        }
      });
    });
  }

  // Submit the form with the token ID.
  stripeTokenHandler(token) {
    // Insert the token ID into the form so it gets submitted to the server
    // this.form.token = token;
    let params = {
      'id': this._propertyService.selectedAccountTopupPurchase.id,
      'AccountTopupPurchase[card_name]' : this.form.card_name,
      'AccountTopupPurchase[stripe_token]': token.id,
      'AccountTopupPurchase[payment_date]': this.form.payment_date
    }
    this._propertyService.stripeCharge(params, ()=>{
        this.router.navigateByUrl('/payment-process')
    });
  }

  goBack() {
    this.location.back();
  }

}
