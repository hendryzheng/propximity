import { Component } from '@angular/core';
import { AccountService } from '../services/account.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(private _accountService: AccountService) {
    this._accountService.checkUserSession();
  }

  ionViewWillEnter(){
    this._accountService.checkUserSession();
  }

  isLoggedIn(){
    return this._accountService.isUserSessionAlive();
  }

}
