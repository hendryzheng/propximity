import { Component, OnInit } from '@angular/core';
import { AccountService } from '../services/account.service';
import { Router } from '@angular/router';
import { UtilitiesService } from '../services/utilities.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private _accountService: AccountService,
              private router: Router,
              private location: Location,
              private _utilitiesService: UtilitiesService,
              private _httpService: HttpService) { }

  ngOnInit() {
  }

  form = {
    username: '',
    password: ''
  };
  loginFailed:boolean = false;

  private callbackSuccess(response: any) {
    localStorage.setItem('userData', JSON.stringify(response.data));
    this._accountService.userData = response.data;
    this._accountService.registerToken(this._accountService.token);
    this.router.navigate(['/tabs'], {replaceUrl: true});
    
  }

  private loginSubmit(){
    this._accountService.checkAppVersion(() => {
      this._utilitiesService.showLoading();
      this._httpService.postRequest(CONFIGURATION.URL.login, this.form).subscribe(response => {
        console.log(response);
        var responseHttp = response;
        this._utilitiesService.hideLoading().then(response => {
          if (responseHttp.status === 'OK') {
            this.callbackSuccess(responseHttp);
          } else {
            this.loginFailed = true;
            this._utilitiesService.alertMessage('Oops! Something happened', responseHttp.message);
          }
        });

      },error => {
        this._utilitiesService.hideLoading();
        console.log(error);
      });
    });
  }

  logForm() {
    console.log(this.form);
    if (this.validateForm()) {
      this.loginSubmit();
    }

  }

  goBack(){
    this.router.navigate(['/tabs'],{replaceUrl: true});
  }

  private validateForm() {
    var validate = true;
    console.log(this.form);
    if (this._utilitiesService.isEmpty(this.form.username)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.password)) {
      validate = false;
    }
    if (!validate) {
      this._utilitiesService.alertMessage('Oops! Something happened', 'Please fill up all fields');
    }
    console.log(validate);
    return validate;
  }

  async forgotPwd() {
    const forgot = await this._utilitiesService.getAlertCtrl().create({
      header: 'Forgot Password?',
      message: "Enter you email address to send a reset link password.",
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            console.log('Send clicked');
            let param = {
              email: data.email
            }
            this._accountService.forgotPassword(param, () => {

            });
            // let toast = this.toastCtrl.create({
            //   message: 'Email was sended successfully',
            //   duration: 3000,
            //   position: 'top',
            //   cssClass: 'dark-trans',
            //   closeButtonText: 'OK',
            //   showCloseButton: true
            // });
            // toast.present();
          }
        }
      ]
    });
    forgot.present();
  }

}
