import { Component, OnInit } from '@angular/core';
import { AccountService } from '../services/account.service';
import { PropertyService } from '../services/property.service';

import * as moment from 'moment';
import { UtilitiesService } from '../services/utilities.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  isTab: boolean = true;
  tabState: any = 'profile';

  form = {
    fullname: '',
    cea_no: '',
    agency: '',
    roles: '',
    email: '',
    password: '',
    confirm_password: '',
    mobile: ''
  }

  showModal: boolean = false;
  publishCredit: any = 0;

  constructor(private _accountService: AccountService,
    private _utilitiesService: UtilitiesService,
    private router: Router,
    private _propertyService: PropertyService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this._accountService.checkUserSession();
    this.myListing();
    this._accountService.getUserDetail(()=>{
      let config = JSON.parse(localStorage.getItem('config'));
      this.publishCredit = config[4].val;
    });

  }

  logout(){
    localStorage.clear();
    this._accountService.userData = [];
    this.router.navigate(['/tabs'], {replaceUrl: true})
  }

  async publishAlert(item) {
    const forgot = await this._utilitiesService.getAlertCtrl().create({
      header: 'Publish',
      message: "Publish Property with active viewing time slot. 1 "+this.publishCredit+" CREDIT ",
      
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Publish',
          handler: data => {
            console.log('Send clicked');
            let param = {
              id: item.id,
              credit: this.publishCredit
            }
            this._accountService.publish(param, () => {
              this._utilitiesService.alertMessageCallback('Success','Listing published successfully !', ()=>{
                this.myListing();
              });
            });
            // let toast = this.toastCtrl.create({
            //   message: 'Email was sended successfully',
            //   duration: 3000,
            //   position: 'top',
            //   cssClass: 'dark-trans',
            //   closeButtonText: 'OK',
            //   showCloseButton: true
            // });
            // toast.present();
          }
        }
      ]
    });
    forgot.present();
  }

  publish(item){
    // this.showModal = true;

  }

  toggleTab(string) {
    this.tabState = string;
  }

  getTopupHistory(){
    return this._accountService.topupHistory;
  }

  openBrowser(title, url){
    this._utilitiesService.openBrowser(title, url);
  }

  myListing() {
    let params = {
      account_id: this._accountService.userData.id
    }
    this._propertyService.getMyListing(params, () => {
      this._accountService.userData = this._propertyService.account_detail;
      this.form.fullname = this._accountService.userData.fullname;
      this.form.cea_no = this._accountService.userData.cea_no;
      this.form.agency = this._accountService.userData.agency;
      this.form.roles = this._accountService.userData.roles;
      this.form.email = this._accountService.userData.email;
      this.form.mobile = this._accountService.userData.mobile;
    });
  }

  formatDate(dt){
    return moment(dt, 'YYYY-MM-DD HH:mm:ss').format('DD MMM');
  }

  updateProfile(){
    if (this.validateForm()) {
      let formSubmission = {
        'account_id' : this._accountService.userData.id,
        'Account[fullname]': this.form.fullname,
        'Account[password]': this.form.password,
        'Account[email]': this.form.email,
        'Account[mobile]': this.form.mobile,
        'Account[cea_no]': this.form.cea_no,
        'Account[agency]': this.form.agency
      };
      this._accountService.updateProfile(formSubmission, () => {
        // this.callbackSuccess();
      });
    }
  }

  private validateForm() {
    var validate = true;
    console.log(this.form);
    if (this._utilitiesService.isEmpty(this.form.fullname)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.roles)) {
      validate = false;
    }
    if(this.form.roles == 'agent'){
      if (this._utilitiesService.isEmpty(this.form.cea_no)) {
        validate = false;
      }
      if (this._utilitiesService.isEmpty(this.form.agency)) {
        validate = false;
      }
    }
    
    if (this._utilitiesService.isEmpty(this.form.email)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.mobile)) {
      validate = false;
    }
    if (this.form.password !== this.form.confirm_password) {
      this._utilitiesService.alertMessage('Validation Error', 'Password not equal with Confirm Password');
      return false;
    }
    if (!validate) {
      this._utilitiesService.alertMessage('Validation Error', 'Please fill up all details');
      return false;
    }
    console.log(validate);
    return validate;
  }

  formatTime(dt){
    return moment(dt, 'YYYY-MM-DD HH:mm:ss').format('hh a');
  }

  edit(item){
    this._propertyService.selectedListing = item;
    let temp = item.available_date_time;
    this._propertyService.availableDateTimeViewing = [];
    for(var i=0;i<temp.length;i++){
      var startDate = moment(temp[i].available_date_from, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD');
      var startTime = moment(temp[i].available_date_from, 'YYYY-MM-DD HH:mm:ss').format('HH:mm');
      var endDate = moment(temp[i].available_date_to, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD');
      var endTime = moment(temp[i].available_date_to, 'YYYY-MM-DD HH:mm:ss').format('HH:mm');
      let data = {
        startDate: moment(temp[i].available_date_from, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD'),
        startDateTime: startDate+' '+startTime+':00',
        startTime: startTime,
        endDate: endDate,
        endDateTime: endDate+ ' '+endTime+':00',
        endTime: endTime,
        display: moment(startDate+' '+startTime, 'YYYY-MM-DD HH:mm').format('DD MMM YYYY HH:mm')+ ' to ' +moment(endDate+ ' '+endTime, 'YYYY-MM-DD HH:mm').format('DD MMM YYYY HH:mm')
      };
      this._propertyService.availableDateTimeViewing.push(data);
    }
    this.router.navigateByUrl('/edit-post');
  }

  getListing() {
    return this._propertyService.myListing;
  }

  getUserData(){
    return this._accountService.userData;
  }
  

}
