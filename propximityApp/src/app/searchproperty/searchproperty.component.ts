import { Component, OnInit } from '@angular/core';
import { PropertyService } from '../services/property.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-searchproperty',
  templateUrl: './searchproperty.component.html',
  styleUrls: ['./searchproperty.component.scss'],
})
export class SearchpropertyComponent implements OnInit {

  constructor(private _propertyService: PropertyService, private modalController: ModalController) { }

  ngOnInit() {}

  ionViewWillEnter(){
    this._propertyService.searchPropertyList = [];
  }
  search(ev){
    if(ev.target.value == ''){
      this._propertyService.searchPropertyList = [];
    }else{
      this._propertyService.searchPropertyList = [];
      this._propertyService.searchPropertyName(ev.target.value, ()=>{

      });
    }
    
  }
  cancel(){
    this._propertyService.searchPropertyList = [];
  }

  close(){
    this.modalController.dismiss();
  }

  getResults(){
    return this._propertyService.searchPropertyList;
    // return this._propertyService.searchPropertyList.filter(
    //   item => item.BLK_NO == '');
  }

  select(item){
    this.modalController.dismiss(item);
  }

}
