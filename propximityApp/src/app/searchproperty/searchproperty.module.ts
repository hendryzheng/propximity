import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { SearchpropertyComponent } from './searchproperty.component';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ],
  entryComponents: [SearchpropertyComponent],
  declarations: [SearchpropertyComponent]
})
export class SearchpropertyModule {}
