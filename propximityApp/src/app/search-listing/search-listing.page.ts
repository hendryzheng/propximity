import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { AccountService } from '../services/account.service';
import { PropertyService } from '../services/property.service';
import { UtilitiesService } from '../services/utilities.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-search-listing',
  templateUrl: './search-listing.page.html',
  styleUrls: ['./search-listing.page.scss'],
})
export class SearchListingPage implements OnInit {

  startDate: any = moment().format('ddd, DD MMM YY');
  startTime: any = '08:00';

  endDate: any = moment(this.startDate, 'YYYY-MM-DD').add(1, 'days').format('YYYY-MM-DD') ;
  endTime: any = '21:00';

  toggleFilter: boolean = true;

  constructor(private _accountService: AccountService,
              private _propertyService: PropertyService,
              private router: Router,
              private _utilitiesService: UtilitiesService) { }

  ngOnInit() {
  }

  toggle(){
    this.toggleFilter = !this.toggleFilter;
  }

  ionViewWillEnter(){
    this._accountService.checkUserSession();
    var params = {};
    if(this._accountService.isUserSessionAlive()){
      params = {
        'account_id' : this._accountService.userData.id
      }
    }
    
    this._propertyService.getListing(params, ()=>{

    });
  }

  isLoggedIn(){
    return this._accountService.isUserSessionAlive();
  }

  getUserData(){
    return this._accountService.userData;
  }

  getListing(){
    return this._propertyService.listing;
  }

  openBrowser(title, url){
    this._utilitiesService.openBrowser(title, url);
  }

  formatDate(dt){
    return moment(dt, 'YYYY-MM-DD HH:mm:ss').format('DD MMM');
  }

  formatTime(dt){
    return moment(dt, 'YYYY-MM-DD HH:mm:ss').format('hh a');
  }

  getListingType(){
    return this._propertyService.listing_categories;
  }

  sendRequest(item){
    this._propertyService.selectedListing = item;
    this.router.navigateByUrl('/send-request');
  }

}
