import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchListingPage } from './search-listing.page';

describe('SearchListingPage', () => {
  let component: SearchListingPage;
  let fixture: ComponentFixture<SearchListingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchListingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchListingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
