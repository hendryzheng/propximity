import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccountService } from './services/account.service';
import { UtilitiesService } from './services/utilities.service';
import { HttpService } from './services/http.service';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { PropertyService } from './services/property.service';
import { ThemeableBrowser } from '@ionic-native/themeable-browser/ngx';
import { Stripe } from '@ionic-native/stripe/ngx';
import { DatePicker } from '@ionic-native/date-picker/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,HttpClientModule],
  providers: [
    AccountService,
    UtilitiesService,
    HttpService,
    PropertyService,
    ThemeableBrowser,
    StatusBar,
    FileTransfer,
    SplashScreen,
    Stripe,
    DatePicker,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
