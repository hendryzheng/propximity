import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { AccountService } from '../services/account.service';

@Component({
  selector: 'app-payment-process',
  templateUrl: './payment-process.page.html',
  styleUrls: ['./payment-process.page.scss'],
})
export class PaymentProcessPage implements OnInit {

  constructor(private location: Location,
              private _accountService: AccountService,
              private router: Router) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this._accountService.getUserDetail(()=>{
      
    })
  }

  goBack(){
    this.location.back();
  }

  home(){
    this.router.navigate(['/tabs'],{replaceUrl:true});
  }

}
