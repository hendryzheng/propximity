import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CreatePostPage } from './create-post.page';
import { SearchpropertyModule } from '../searchproperty/searchproperty.module';
import { LocationModule } from '../location/location.module';
import { ListingDateTimeInputModule } from '../listing-date-time-input/listing-date-time-input.module';

const routes: Routes = [
  {
    path: '',
    component: CreatePostPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchpropertyModule,
    LocationModule,
    ListingDateTimeInputModule,
    RouterModule.forChild(routes)
  ],
  // entryComponents: [LocationComponent, SearchpropertyComponent, ListingDateTimeInputPage],
  declarations: [CreatePostPage]
})
export class CreatePostPageModule {}
