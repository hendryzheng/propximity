import { Component, OnInit, NgZone } from '@angular/core';
import { PropertyService } from '../services/property.service';
import { ModalController } from '@ionic/angular';
import { LocationComponent } from '../location/location.component';
import { UtilitiesService } from '../services/utilities.service';
import { SearchpropertyComponent } from '../searchproperty/searchproperty.component';
import { ListingDateTimeInputPage } from '../listing-date-time-input/listing-date-time-input.page';
import { AccountService } from '../services/account.service';
import { Router } from '@angular/router';

declare var cordova: any;

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.page.html',
  styleUrls: ['./create-post.page.scss'],
})
export class CreatePostPage implements OnInit {

  form = {
    listing_type: '',
    property_type: '',
    hdb_type: '',
    property_name: '',
    blk_no: '',
    road_name: '',
    building: '',
    address: '',
    postal: '',
    lat: '',
    lng: '',
    highlight: '',
    location: '',
    floor_area: '',
    number_of_rooms: '',
    url: '',
    vacant_type: '',
    asking_price: '',
    com_agent_profile: '',
    available_date_viewing: '',
    account_id: 0,
    expected_close: ''
  };
  label_display: any = 'Property Name';
  vacant = [
    { val: 'Vacant', isChecked: false },
    { val: 'Tenant', isChecked: false },
    { val: 'Owner Stay', isChecked: false }
  ];
  highlights_arr = [];

  inAppWindow: any = null;
  highlights: any = [];

  location_display: '';

  constructor(private _propertyService: PropertyService,
    private _utilitiesService: UtilitiesService,
    private _accountService: AccountService,
    private modalController: ModalController,
    private router: Router,
    private zone: NgZone,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this._accountService.checkUserSession();
    this._propertyService.getHighlights(()=>{
      this.highlights = this._propertyService.highlights;
    })
    this.form.account_id = this._accountService.userData.id;
  }
  
  propertyTypeChange(ev){
    if(this.form.property_type == 'hdb'){
      this.label_display = 'HDB block and street name';
    }else{
      this.label_display = 'Property Name';
    }
  }

  selectHighlight(key, item){
    if(this.highlights[key].selected == 0){
      this.highlights[key].selected = 1;
    }else{
      this.highlights[key].selected = 0;

    }
  }

  getListingType() {
    return this._propertyService.listing_categories;
  }

  getDistrict() {
    return this._propertyService.districts;
  }

  submit() {
    var highlights = '';
    for(var i=0;i<this.highlights.length;i++){
      if(this.highlights[i].selected == 1){
        highlights += this.highlights[i].keyword+',';
      }
    }
    if(highlights !== ''){
      this.form.highlight = highlights.substring(0, highlights.length - 1);;
    }
    if (this.validateForm()) {
      let formSubmission = {
        'Listing[listing_type]': this.form.listing_type,
        'Listing[property_type]': this.form.property_type,
        'Listing[property_name]': this.form.property_name,
        'Listing[hdb_type]': this.form.hdb_type,
        'Listing[blk_no]': this.form.blk_no,
        'Listing[road_name]': this.form.road_name,
        'Listing[building]': this.form.building,
        'Listing[address]': this.form.address,
        'Listing[postal]': this.form.postal,
        'Listing[lat]': this.form.lat,
        'Listing[lng]': this.form.lng,
        'Listing[highlight]': this.form.highlight,
        'Listing[location]': this.form.location,
        'Listing[floor_area]': this.form.floor_area,
        'Listing[no_of_rooms]': this.form.number_of_rooms,
        'Listing[url]': this.form.url,
        'Listing[vacant_type]': this.form.vacant_type,
        'Listing[asking_price]': this.form.asking_price,
        'Listing[com_agent_profile]': this.form.com_agent_profile,
        'Listing[account_id]': this.form.account_id,
        'Listing[expected_close]': this.form.expected_close,
        'available_date_viewing': JSON.stringify(this._propertyService.availableDateTimeViewing),
      };
      this._propertyService.addListing(formSubmission, () => {
        var msg = '';
        if(this._propertyService.availableDateTimeViewing.length == 0){
          msg = 'Listing created! Please remember to update viewing time when ready';
        }else{
          msg = 'Listing created! All the best to you';
        }
        this._utilitiesService.alertMessageCallback('Success', msg, () => {
          this.form = {
            listing_type: '',
            property_type: '',
            property_name: '',
            hdb_type: '',
            blk_no: '',
            road_name: '',
            building: '',
            address: '',
            postal: '',
            lat: '',
            lng: '',
            highlight: '',
            location: '',
            floor_area: '',
            number_of_rooms: '',
            url: '',
            vacant_type: '',
            asking_price: '',
            com_agent_profile: '',
            available_date_viewing: '',
            account_id: 0,
            expected_close: ''
          };
          this.router.navigate(['/tabs/search-listing'], { replaceUrl: true });
        });
      });
    }

  }

  browse() {
    var CustomHeader = {
        css: '#customheader { width:100px;height:30px;background:#ccc;border-radius:5px;position:fixed;bottom:0;left:0;color:orange;z-index:9999; font-weight:bold;}',
        html: ()=> {
          var code = 'var div = document.createElement("BUTTON");div.id = "customheader";div.innerHTML="COPY URL";if (document.body.firstChild){ document.body.insertBefore(div, document.body.firstChild); }else { document.body.appendChild(div); }document.getElementById("customheader").addEventListener("click", ()=>{ alert("clicked");window.location.href += "?close=true"; });';
          return code;
      }
    };
    this.inAppWindow = cordova.InAppBrowser.open('https://propertyguru.com.sg', '_blank', 'location=yes');
    // Listen to the events, we need to know when the page is completely loaded
    this.inAppWindow.addEventListener('loadstop',  event=> {
      var code = CustomHeader.html();
      // Inject your JS code!
      this.inAppWindow.executeScript({
        code: code
      },  ()=> {
        console.log("injected (callback).");
      });
      // Inject CSS!
      this.inAppWindow.insertCSS({
        code: CustomHeader.css
      },  ()=> {
        console.log("CSS inserted!");
      });
      if(event.url.indexOf('close') > -1) {
        // this.form.url = event.url;
        
        console.log(event.url);
        
        this.inAppWindow.close();
        this.zone.run(()=>{
          this.form.url = event.url.replace('?close=true','');
        });
      }
    });
  }

  private validateForm() {
    var validate = true;
    console.log(this.form);
    var vacant = '';
    for(var i=0;i<this.vacant.length;i++){
      if(this.vacant[i].isChecked){
        vacant += this.vacant[i].val+',';
      }
    }
    if(vacant !== ''){
      this.form.vacant_type = vacant.substring(0, vacant.length - 1);;
    }
    if (this._utilitiesService.isEmpty(this.form.property_name)) {
      validate = false;
      this._utilitiesService.alertMessage('Validation Error', 'Property name cannot be empty');
      return;
    }
    if (this._utilitiesService.isEmpty(this.form.address)) {
      validate = false;
      this._utilitiesService.alertMessage('Validation Error', 'Address cannot be empty');
      return;
    }

    // if (this._utilitiesService.isEmpty(this.form.highlight)) {
    //   validate = false;
    //   this._utilitiesService.alertMessage('Validation Error', 'Highlight cannot be empty');
    //   return;
    // }
    if (this._utilitiesService.isEmpty(this.form.listing_type)) {
      validate = false;
      this._utilitiesService.alertMessage('Validation Error', 'Listing type cannot be empty');
      return;
    }
    if (this._utilitiesService.isEmpty(this.form.property_type)) {
      validate = false;
      this._utilitiesService.alertMessage('Validation Error', 'Property type cannot be empty');
      return;
    }
    if (this._utilitiesService.isEmpty(this.form.location)) {
      validate = false;
      this._utilitiesService.alertMessage('Validation Error', 'Location cannot be empty');
      return;
    }
    if (this._utilitiesService.isEmpty(this.form.vacant_type)) {
      validate = false;
      this._utilitiesService.alertMessage('Validation Error', 'Vacant Type cannot be empty');
      return;
    }
    if (this._utilitiesService.isEmpty(this.form.asking_price)) {
      validate = false;
      this._utilitiesService.alertMessage('Validation Error', 'Asking Price cannot be empty');
      return;
    }
    // if (this._propertyService.availableDateTimeViewing.length < 1) {
    //   validate = false;
    //   this._utilitiesService.alertMessage('Validation Error', 'Available Date Time Viewing cannot be empty');
    //   return;
    // }

    if (!validate) {
      this._utilitiesService.alertMessage('Validation Error', 'Please fill up all details');
      return false;
    }
    console.log(validate);
    return validate;
  }

  async presentModal() {
    console.log(this.form);
    if (this.form.property_type == '') {
      this._utilitiesService.alertMessage('Property type empty', 'Please select property type');
      return;
    }
    this._propertyService.selectedPropertyType = this.form.property_type;
    const modal = await this.modalController.create({
      component: LocationComponent
    });
    modal.onDidDismiss()
      .then((dataResp: any) => {
        console.log(dataResp);
        let data = dataResp.data;
        this.zone.run(() => {
          this.form.location = data.key;
          this.location_display = data.value;
        })
      });
    return await modal.present();
  }

  async presentModalProperty() {
    const modal = await this.modalController.create({
      component: SearchpropertyComponent
    });
    modal.onDidDismiss()
      .then((dataResp: any) => {
        console.log(dataResp);
        let data = dataResp.data;
        this.zone.run(() => {
          if(typeof data.BUILDING !== 'undefined'){
            this.form.property_name = data.BUILDING;
            this.form.blk_no = data.BLK_NO;
            this.form.address = data.ADDRESS;
            this.form.road_name = data.ROAD_NAME;
            this.form.building = data.BUILDING;
            this.form.postal = data.POSTAL;
            this.form.lat = data.LATITUDE;
            this.form.lng = data.LONGITUDE;
          }
          
          console.log(this.form);
        })
      });
    return await modal.present();
  }

  async presentModalInputTime() {
    const modal = await this.modalController.create({
      component: ListingDateTimeInputPage
    });
    modal.onDidDismiss()
      .then((dataResp: any) => {

      });
    return await modal.present();
  }

  getAvailableTimeViewing() {
    return this._propertyService.availableDateTimeViewing;
  }

  remove(key) {
    this._propertyService.availableDateTimeViewing.splice(key, 1);
  }

  getUserData(){
    return this._accountService.userData;
  }

}
