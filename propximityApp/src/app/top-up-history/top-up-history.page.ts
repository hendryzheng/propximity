import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { UtilitiesService } from '../services/utilities.service';
import { Router } from '@angular/router';
import { AccountService } from '../services/account.service';
import { PropertyService } from '../services/property.service';
import { HttpService } from '../services/http.service';

@Component({
  selector: 'app-top-up-history',
  templateUrl: './top-up-history.page.html',
  styleUrls: ['./top-up-history.page.scss'],
})
export class TopUpHistoryPage implements OnInit {

  form = {
    fullname: '',
    cea_no: '',
    agency: '',
    roles: '',
    email: '',
    password: '',
    confirm_password: '',
    mobile: ''
  }

  showModal: boolean = false;
  publishCredit: any = 0;

  constructor(private _accountService: AccountService,
    private _utilitiesService: UtilitiesService,
    private location: Location,
    private router: Router,
    private _propertyService: PropertyService) { }

  ngOnInit() {
  }

  goBack(){
    this.location.back();
  }

  ionViewWillEnter() {
    this._accountService.checkUserSession();
    this._accountService.getUserDetail(()=>{
      let config = JSON.parse(localStorage.getItem('config'));
      this.publishCredit = config[4].val;
    });

  }

  logout(){
    localStorage.clear();
    this._accountService.userData = [];
    this.router.navigate(['/tabs'], {replaceUrl: true})
  }


  getTopupHistory(){
    return this._accountService.topupHistory;
  }

  openBrowser(title, url){
    this._utilitiesService.openBrowser(title, url);
  }


  updateProfile(){
    if (this.validateForm()) {
      let formSubmission = {
        'account_id' : this._accountService.userData.id,
        'Account[fullname]': this.form.fullname,
        'Account[password]': this.form.password,
        'Account[email]': this.form.email,
        'Account[mobile]': this.form.mobile,
        'Account[cea_no]': this.form.cea_no,
        'Account[agency]': this.form.agency
      };
      this._accountService.updateProfile(formSubmission, () => {
        // this.callbackSuccess();
      });
    }
  }

  private validateForm() {
    var validate = true;
    console.log(this.form);
    if (this._utilitiesService.isEmpty(this.form.fullname)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.roles)) {
      validate = false;
    }
    if(this.form.roles == 'agent'){
      if (this._utilitiesService.isEmpty(this.form.cea_no)) {
        validate = false;
      }
      if (this._utilitiesService.isEmpty(this.form.agency)) {
        validate = false;
      }
    }
    
    if (this._utilitiesService.isEmpty(this.form.email)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.mobile)) {
      validate = false;
    }
    if (this.form.password !== this.form.confirm_password) {
      this._utilitiesService.alertMessage('Validation Error', 'Password not equal with Confirm Password');
      return false;
    }
    if (!validate) {
      this._utilitiesService.alertMessage('Validation Error', 'Please fill up all details');
      return false;
    }
    console.log(validate);
    return validate;
  }



  getUserData(){
    return this._accountService.userData;
  }
  


}
