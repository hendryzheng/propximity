export let CONFIGURATION = {
    // apiEndpoint: 'http://localhost/propximity/',
    // apiEndpoint: 'https://misa.com.my/',
    // apiEndpoint: 'https://velintech.com/projects/propximity/',
    apiEndpoint: 'https://propximity.sg/',
    // apiEndpoint: 'https://www.candyb.co/back/',
    // apiEndpoint: 'https://wellous.com/backend/api',
    //prod
    // apiEndpoint: 'https://app-api.emcapital2.org/',
    
    // apiEndpoint: 'https://binder.bwint.org/cms/',

    //staging
    // apiEndpoint: 'http://121.121.47.174/epc_web/',
    URL: {
        login: 'api/login',
        register: 'api/register',
        registerToken: 'api/tokenRegistration',
        forgotPassword: 'api/forgotPassword',
        getAppVersion: 'api/getAppVersion',
        uploadImage: 'api/uploadImages',
        uploadImageNew: 'api/uploadImageNew',
        updateRecordNew: 'api/updateRecordNew',
        deleteImages: 'api/deleteImages',
        getNotifications: 'api/getNotifications',
        addListing: 'api/addListing',
        deleteListing: 'api/deleteListing',
        getListing: 'api/getListing',
        getHighlights: 'api/getHighlights',
        updateListing: 'api/updateListing',
        myListing: 'api/myListing',
        getUserDetail: 'api/getUserDetail',
        updateProfile: 'api/updateProfile',
        getTopupPackage: 'api/getTopupPackages',
        sendViewingRequest: 'api/sendViewingRequest',
        getViewingRequest: 'api/getViewingRequest',
        createTopupPurchase: 'api/createTopupPurchase',
        stripeCharge: 'api/stripeCharge',
        getAccountDetail: 'api/getUserDetail',
        getConfig: 'api/getConfig',
        getTopupHistory: 'api/getTopupHistory',
        publish: 'api/publish',
        addTagAgents: 'api/addTagAgents',
        getTagAgents: 'api/getTagAgents'
    },
    TOKEN : {
        CLIENT_ID: 'ypPZ23gL6p',
        CLIENT_TOKEN: 'clzN2WEHTP'
    },
    GET_IP:'https://freegeoip.net/json/'

}