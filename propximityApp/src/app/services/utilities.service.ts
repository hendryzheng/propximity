import { Injectable } from '@angular/core';
import { AlertController, Platform, LoadingController } from '@ionic/angular';
import { ThemeableBrowser, ThemeableBrowserOptions, ThemeableBrowserObject } from '@ionic-native/themeable-browser/ngx';


@Injectable()
export class UtilitiesService {

    constructor(public alertCtrl: AlertController,
        public plt: Platform,
        private themeableBrowser: ThemeableBrowser,
        public _loadingController: LoadingController) {
            var currentTime = new Date();
            var year = currentTime.getFullYear();

            for (var i = year - 5; i <= year; i++) {
                var temp = { val: i };
                this.years.push(temp);
            }
    }

    private loading = null;
    isLoading = false;
    showSearchBar: boolean = false;
    showHeaderLink: boolean = true;
    showSearch = true;
    months = [
        {
            val: '01',
            text: 'Jan'
        },
        {
            val: '02',
            text: 'Feb'
        },
        {
            val: '03',
            text: 'Mar'
        },
        {
            val: '04',
            text: 'Apr'
        },
        {
            val: '05',
            text: 'May'
        },
        {
            val: '06',
            text: 'Jun'
        },
        {
            val: '07',
            text: 'Jul'
        },
        {
            val: '08',
            text: 'Aug'
        },
        {
            val: '09',
            text: 'Sep'
        },
        {
            val: '10',
            text: 'Oct'
        },
        {
            val: '11',
            text: 'Nov'
        },
        {
            val: '12',
            text: 'Dec'
        },
    ];

    years = [];

    isEmpty(param: any) {
        if (param === '' || typeof param === 'undefined' || param === 'null' || param === null) {
            return true;
        } else {
            return false;
        }
    }

    formatFloat(value: any) {
        return parseFloat(value).toFixed(2);
    }

    formatWhatsappNo(mobile_no){
        var mobile_no = mobile_no.replace('+','');
        if(mobile_no.charAt(0) !== '6' && mobile_no.charAt(1) !== '5'){
            mobile_no = '65'+mobile_no;
        }
        return mobile_no;
    }

    sizeOf(bytes, obj) {
        if(obj !== null && obj !== undefined) {
            switch(typeof obj) {
            case 'number':
                bytes += 8;
                break;
            case 'string':
                bytes += obj.length * 2;
                break;
            case 'boolean':
                bytes += 4;
                break;
            case 'object':
                var objClass = Object.prototype.toString.call(obj).slice(8, -1);
                if(objClass === 'Object' || objClass === 'Array') {
                    for(var key in obj) {
                        if(!obj.hasOwnProperty(key)) continue;
                        this.sizeOf(bytes, obj[key]);
                    }
                } else bytes += obj.toString().length * 2;
                break;
            }
        }
        return bytes;
    }

    formatByteSize(bytes) {
        if(bytes < 1024) return bytes + " bytes";
        else if(bytes < 1048576) return(bytes / 1024).toFixed(3) + " KiB";
        else if(bytes < 1073741824) return(bytes / 1048576).toFixed(3) + " MiB";
        else return(bytes / 1073741824).toFixed(3) + " GiB";
    }

    memorySizeOf(obj) {
        const bytes = 0;
        return this.sizeOf(bytes,obj);
    }


    async flashAlertMessage(title: string, message: string) {
        const alertController = document.querySelector('ion-alert-controller');
        await alertController.componentOnReady();
        const alert = await alertController.create({
            header: title,
            message: message,
            buttons: [
                {
                    text: 'OK',
                    handler: data => {
                        return true;
                    }
                }
            ]
        })
        return alert.present();
    }

    getAlertCtrl() {
        return this.alertCtrl;
    }

    

    async alertMessage(title: string, message: string) {
        const alertController =this.alertCtrl;
        const alert = await alertController.create({
                header: title,
                message: message,
                buttons: [
                    {
                        text: 'OK',
                        handler: data => {
                            console.log('OK clicked');
                        }
                    },
                    
                ]
            });
        return alert.present();
    }

    formatDate(dateString: any) {
        if(this.plt.is('ios')){
            var t = dateString.split(/[- :]/);
            
            // Apply each element to the Date function
            var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
            var date = new Date(d);
        }else{
            var date = new Date(dateString);
        }
        
        var monthNames = [
            "Jan", "Feb", "Mar",
            "Apr", "May", "Jun", "Jul",
            "Aug", "Sep", "Oct",
            "Nov", "Dec"
        ];

        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        return day + ' ' + monthNames[monthIndex] + ' ' + year;
    }

    async showLoading() {
        this.isLoading = true;
        return await this._loadingController.create({
          duration: 5000,
        }).then(a => {
          a.present().then(() => {
            console.log('presented');
            if (!this.isLoading) {
              a.dismiss().then(() => console.log('abort presenting'));
            }
          });
        });
      }
      async showLoadingSync() {
        this.isLoading = true;
        return await this._loadingController.create({
          message: 'Please wait, while the data is being synced to server',
          duration: 500000,
        }).then(a => {
          a.present().then(() => {
            console.log('presented');
            if (!this.isLoading) {
              a.dismiss().then(() => console.log('abort presenting'));
            }
          });
        });
      }

      async showLoadingSyncImages() {
        this.isLoading = true;
        return await this._loadingController.create({
          message: 'Please wait, while the images data is being uploaded to server',
          duration: 500000,
        }).then(a => {
          a.present().then(() => {
            console.log('presented');
            if (!this.isLoading) {
              a.dismiss().then(() => console.log('abort presenting'));
            }
          });
        });
      }
    
      async hideLoading() {
        
        return await this._loadingController.dismiss().then(() =>{this.isLoading = false;});
      }

    async alertMessageCallback(title: string, message: string, callback: () => void) {
        const alertController = this.alertCtrl;
        const alert = await alertController.create({
                header: title,
                message: message,
                buttons: [
                    {
                        text: 'OK',
                        handler: data => {
                            console.log('OK clicked');
                            callback();
                        }
                    }
                ]
            });
        return alert.present();
    }

    openBrowser(title, url) {
        if (this.plt.is('ios')) {
            var options: ThemeableBrowserOptions = {
                statusbar: {
                    color: '#ffffffff'
                },
                toolbar: {
                    height: 30,
                    color: '#cccccc'
                },
                title: {
                    color: '#ffffffff',
                    showPageTitle: true,
                    staticText: title
                },
                closeButton: {
                    wwwImage: 'assets/img/close.png',
                    align: 'left',
                    event: 'closePressed'
                },
                backButtonCanClose: true
            };
            const browser: ThemeableBrowserObject = this.themeableBrowser.create(url, '_blank', options);
            // this.showLoading();
            // browser.on('loadstart').subscribe(event => {
            //     console.log("loadstart");
            //     this.hideLoading().then(resp => {
            //         browser.show();
            //     });
            // });
            // browser.on('closePressed').subscribe(event => {
            //     console.log("closePressed");
            // })
        } else {
            this.openBrowserAndroid(title, url);
        }


    }

    openBrowserAndroid(title, url) {
        var options: ThemeableBrowserOptions = {
            statusbar: {
                color: '#ffffffff'
            },
            toolbar: {
                height: 30,
                color: '#cccccc'
            },
            title: {
                color: '#ffffffff',
                showPageTitle: true,
                staticText: title
            },
            closeButton: {
                wwwImage: 'assets/img/close.png',
                align: 'left',
                event: 'closePressed'
            },
            backButtonCanClose: true
        };
        const browser: ThemeableBrowserObject = this.themeableBrowser.create(url, '_blank', options);

    }

    handleError(error: any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body: any = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
            // alert(body.message);
            let alertTitle = 'Oops ! Ada yg tidak beres !';
            this.alertMessage(alertTitle, body.message);

        } else {
            errMsg = error.message ? error.message : error.toString();
            // this.alertMessage('Oops ! Something Happened', errMsg);
            let alertTitle = 'Oops ! Ada yg tidak beres !';
            this.alertMessage(alertTitle, errMsg);
        }
    }

    getMonths() {
        return this.months;
    }

    getYears() {
        return this.years;
    }

    getCurrentMonth(){
        let currentTime = new Date();
        var month = currentTime.getMonth() + 1;
        return month;
    }

    getCurrentYear(){
        let currentTime = new Date();
        var year = currentTime.getFullYear();
        return year;
    }

}