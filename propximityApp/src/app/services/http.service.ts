import { Injectable } from '@angular/core';
import { HttpRequest, HttpEventType, HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

import { catchError, tap } from 'rxjs/operators';
import { CONFIGURATION } from './config.service';
import { FileTransferObject, FileTransfer, FileUploadOptions } from '@ionic-native/file-transfer/ngx';

declare var device: any;
declare var navigator: any;

@Injectable()
export class HttpService {

    public uploadProgress: BehaviorSubject<number> = new BehaviorSubject<number>(0);
    public downloadProgress: BehaviorSubject<number> = new BehaviorSubject<number>(0);

    constructor(public httpClient: HttpClient, public http: HttpClient, public transfer: FileTransfer) {

    }

    deviceModel: string = '';
    ipInfo: any = [];

    private ObjecttoParams(obj) {
        var p = [];
        for (var key in obj) {
            p.push(key + '=' + encodeURIComponent(obj[key]));
        }
        return p.join('&');
    };

    async fetchIp() {
        this.http.get(CONFIGURATION.GET_IP).pipe(
            map((response: any) => response),
            catchError(this.handleError('fetchIp', []))
        ).subscribe((res: Response) => {
            let stringify = JSON.stringify(res);
            localStorage.setItem('ipInfo', JSON.stringify(res));
            console.log(stringify);
            this.ipInfo = res;
        });
    }

    private getLoginAccount() {
        var userData = localStorage.getItem('userData');
        console.log(userData);
        if (userData) {
            return JSON.parse(userData);
        } else {
            return false;
        }
    }


    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.status}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
    /** Log a HeroService message with the MessageService */
    private log(message: string) {
        console.log(message);
    }



    getRequest(url: string) {
        // url += '?client_id=' + CONFIGURATION.TOKEN.CLIENT_ID + '&client_token=' + CONFIGURATION.TOKEN.CLIENT_TOKEN;
        let params = {
            'lang': localStorage.getItem('content_language')
        };
        let stringParams = this.ObjecttoParams(params);
        return this.http.get(CONFIGURATION.apiEndpoint + url + '?' + stringParams)
            .pipe(
                map((response: any) => response),
                catchError(this.handleError('getRequest : ' + url, []))
            );
    }

    getRequestPlain(url: string) {
        // url += '?client_id=' + CONFIGURATION.TOKEN.CLIENT_ID + '&client_token=' + CONFIGURATION.TOKEN.CLIENT_TOKEN;
        return this.http.get(url).pipe(
            map((response: any) => response),
            catchError(this.handleError('getRequestPlain : ' + url, []))
        );
    }

    postRequestProgress(url, formData) {
        let req = new HttpRequest('POST', url, formData, {
            // responseType: 'arraybuffer',
            reportProgress: true
        });
        console.log(formData);

        return this.httpClient.request(req).pipe(
            map(event => {
                // progress
                if (event.type === HttpEventType.DownloadProgress) {
                    console.log(event.loaded, event.total);
                    // event.loaded = bytes transfered 
                    // event.total = "Content-Length", set by the server

                    const percentage = 100 / event.total * event.loaded;
                    console.log(percentage);
                }

                // finished
                if (event.type === HttpEventType.Response) {
                    console.log(event.body);
                }
                console.log(event);
            }),
            tap(message => console.log(message)),
            // last();
        );
    }

    getStatusMessage(event) {

        let status;

        switch (event.type) {
            case HttpEventType.Sent:
                return `Uploading Files`;

            case HttpEventType.UploadProgress:
                status = Math.round(100 * event.loaded / event.total);
                this.uploadProgress.next(status);
                return `Files are ${status}% uploaded`;

            case HttpEventType.DownloadProgress:
                status = Math.round(100 * event.loaded / event.total);
                this.downloadProgress.next(status); // NOTE: The Content-Length header must be set on the server to calculate this
                return `Files are ${status}% downloaded`;

            case HttpEventType.Response:
                return `Done`;

            default:
                return `Something went wrong`
        }
    }

    postRequest(url: string, params: any) {
        // if (typeof device !== 'undefined') {
        //     this.deviceModel = device.model + " " + device.platform + " " + device.version;
        // }

        // let sessionEncrypted = this._encryptionService.encrypt(paramSession);
        let httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/x-www-form-urlencoded'
            })
        }

        // let options = new RequestOptions({ headers: headers });
        params['nav_agent'] = navigator.userAgent + ' | ' + navigator.platform + ' | ' + navigator.appVersion;
        params['ip_address'] = this.ipInfo.ip;
        params['agent_ip_obj'] = localStorage['ipInfo'];
        params['language'] = localStorage['content_language'];
        params['category'] = localStorage['category'];
        params['client_id'] = CONFIGURATION.TOKEN.CLIENT_ID;
        params['client_token'] = CONFIGURATION.TOKEN.CLIENT_TOKEN;
        params['version'] = localStorage.getItem('version');
        params['isIos'] = localStorage.getItem('isIos');
        if (this.getLoginAccount()) {
            params['account_id'] = this.getLoginAccount().id;
            // let roles = this.getLoginAccount().roles;
            // if(roles){
            //     params['roles'] = roles;
            // }else{
            //     params['roles'] = 'merchandiser';
            // }
        }
        return this.http.post(CONFIGURATION.apiEndpoint + url, this.ObjecttoParams(params), httpOptions)
            .pipe(
                map((response: any) => response),
                catchError(this.handleError('postRequest : ' + url, []))
            );
    }

    async asyncPostRequest(url: string, params: any) {
        // if (typeof device !== 'undefined') {
        //     this.deviceModel = device.model + " " + device.platform + " " + device.version;
        // }

        // let sessionEncrypted = this._encryptionService.encrypt(paramSession);
        // let headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/x-www-form-urlencoded'
            })
        }
        // let options = new RequestOptions({ headers: headers });
        params['nav_agent'] = navigator.userAgent + ' | ' + navigator.platform + ' | ' + navigator.appVersion;
        params['ip_address'] = this.ipInfo.ip;
        params['agent_ip_obj'] = localStorage['ipInfo'];
        params['language'] = localStorage['content_language'];
        params['category'] = localStorage['category'];
        params['client_id'] = CONFIGURATION.TOKEN.CLIENT_ID;
        params['client_token'] = CONFIGURATION.TOKEN.CLIENT_TOKEN;
        params['version'] = localStorage.getItem('version');
        params['isIos'] = localStorage.getItem('isIos');
        if (this.getLoginAccount()) {
            params['merchandiser_id'] = this.getLoginAccount().id;
        }
        return this.http.post(CONFIGURATION.apiEndpoint + url, this.ObjecttoParams(params), httpOptions)
            .pipe(
                map((response: any) => response),
                catchError(this.handleError('postRequest : ' + url, []))
            );
    }

    uploadImage(URL, newImage, params, callback: () => void) {
        // console.log('new image path is: ' + newImage);
        const fileTransfer: FileTransferObject = this.transfer.create();
        const uploadOpts: FileUploadOptions = {
            fileKey: 'file',
            fileName: newImage.substr(newImage.lastIndexOf('/') + 1),
            params: params
        };

        fileTransfer.upload(newImage, URL, uploadOpts)
            .then((data) => {
                console.log(data);
                let respData = JSON.parse(data.response);
                console.log(respData);
                //  this.fileUrl = this.respData.fileUrl;
                callback();
            }, (err) => {
                console.log(err);
            });
    }

}