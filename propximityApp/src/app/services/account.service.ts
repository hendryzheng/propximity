import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

import { UtilitiesService } from './utilities.service';
import { CONFIGURATION } from './config.service';
import { Platform } from '@ionic/angular';

declare var navigator: any;
declare var FileTransfer: any;
declare var FileUploadOptions: any;
declare var cordova: any;

@Injectable()
export class AccountService {

    userData: any = null;
    appVersion: any = '';
    version: any = '';
    token: any = '';
    available_reward: any = '0.00';
    banner: any = [];
    notifications: any = [];
    notificationViewingRequest: any = [];
    notificationTagAgents: any = [];
    topupHistory: any = [];
    constructor(private _httpService: HttpService,
        public platform: Platform,
        private _utilitiesService: UtilitiesService) {
        // var userData = localStorage.getItem('userData');
        // console.log(userData);
    }

    isUserSessionAlive() {

        // console.log('-- ACCOUNTSERVICE isUserSessionAlive : ' + this.userData + ' --');
        if (this.userData === null) {
            return false;
        } else {
            return true;
        }
    }

    checkUserSession() {
        var userData = localStorage.getItem('userData');
        console.log(userData);
        if (!this._utilitiesService.isEmpty(userData)) {
            this.userData = JSON.parse(userData);
        }else{
            this.userData = null;
        }
    }

    getNotifications(params, callback: ()=>void){
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.getNotifications, params).subscribe(response => {
            this._utilitiesService.hideLoading().then(res => {
                if (response.status === 'OK') {
                    this.notificationTagAgents = response.data.tag_agents;
                    this.notificationViewingRequest = response.data.viewing_request;
                    // console.log(this.notifications);
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        }, error => {
            console.log('**error** --CURRENT RECORD UPDATED--');
            this._utilitiesService.hideLoading();
        });
    }

    registerToken(loginData?: any) {
        var merchandiser_id = '';
        if (this._utilitiesService.isEmpty(loginData)) {
            this.checkUserSession();
            if (!this._utilitiesService.isEmpty(this.userData)) {
                
                merchandiser_id = this.userData.id;
            }
        }


        let post = {
            token: this.token,
            merchandiser_id: merchandiser_id
        }
        this._httpService.postRequest(CONFIGURATION.URL.registerToken, post).subscribe(response => {
            // console.log(response);
            if (response.status === 'OK') {

            } else {
                // this._utilitiesService.alertMessage('Oops! Something happened',response.message);
            }
        }, error => {
            console.log(error);
        });

    }

    register(params, callback: ()=>void){
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.register, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this._utilitiesService.alertMessage('Success !', response.message).then(resolve => {
                        callback();
                    });
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    updateProfile(params,callback: ()=>void){
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.updateProfile, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this._utilitiesService.alertMessage('Success !', response.message).then(resolve => {
                        callback();
                    });
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }
    

    async alertForgotPasswordSuccess(msg, callback: () => void){
        const confirm = await this._utilitiesService.getAlertCtrl().create({
            header: 'Successful',
            message: msg,
            buttons: [
                {
                    text: 'OK',
                    handler: () => {
                        callback();
                    }
                }
            ]
        });
        confirm.present();
    }

    forgotPassword(param: any, callback: () => void) {
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.forgotPassword, param).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this.alertForgotPasswordSuccess(response.message, callback);
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    publish(param: any, callback: () => void) {
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.publish, param).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    checkAppVersion(callback: () => void) {

        if (typeof cordova !== 'undefined') {
            this._utilitiesService.showLoading();
            let param = {
                'ios': this.platform.is('ios')
            };
            this._httpService.postRequest(CONFIGURATION.URL.getAppVersion, param)
                .subscribe(response => {
                    this._utilitiesService.hideLoading().then(resolve => {
                        document.addEventListener('deviceready', () => {
                            cordova.getAppVersion.getVersionNumber(version => {
                                console.log(version);
                                this.version = version;
                                localStorage.setItem('version', this.version);
                                if (response.data.version !== version) {
                                    this._utilitiesService.alertMessageCallback('App version outdated', 'Your app version is outdated. Please update your app', () => {
                                        localStorage.clear();
                                        this.userData = [];
                                        let ref = cordova.InAppBrowser.open(response.data.url, '_system', 'location=yes');
                                        ref.addEventListener('loadstop', event => {
                                            ref.show();
                                        });

                                    });

                                } else {
                                    callback();
                                }
                            });
                        });


                    });

                },error => {
                    this._utilitiesService.hideLoading().then(resolve => {
                        localStorage.clear();
                        // this._utilitiesService.handleError(error);
                    });
                    console.log(error);
                });
        } else {
            callback();
        }
    }

    getLoginAccount() {
        // var userData = this.userData;
        if (!this._utilitiesService.isEmpty(this.userData)) {
            return this.userData;
        } else {
            return '';
        }
    }

    getConfig(){

    }

    getUserDetail(callback: ()=>void){
        // this._utilitiesService.showLoading();
        let param = {
            account_id: this.userData.id
        }
        this._httpService.postRequest(CONFIGURATION.URL.getAccountDetail, param).subscribe(response => {

            // this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this.userData = response.data.user;
                    let config = response.data.config;
                    this.topupHistory = response.data.topup_history;
                    localStorage.setItem('config', JSON.stringify(config));
                    localStorage.setItem('userData', JSON.stringify(this.userData));
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            // });

        },error => {
            // this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    getTopupHistory(callback: ()=>void){
        this._utilitiesService.showLoading();
        let param = {
            account_id: this.userData.id
        }
        this._httpService.postRequest(CONFIGURATION.URL.getTopupHistory, param).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this.topupHistory = response.data;
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    getUserId() {
        if (this.userData !== null) {
            return this.userData.id;
        }
        return '';
    }

    getUserType() {
        var userData = this.userData;
        if (userData !== null) {
            return userData.user_type;
        }
        return '';
    }

    isCorporate() {
        var userData = this.userData;
        if (userData !== null) {
            return userData.user_type === 'corporate';
        }
        return false;
    }

    uploadImage(imageUrl: any, callback: () => void, URL) {

        var options = new FileUploadOptions();
        options.fileKey = 'file';
        options.fileName = 'myphoto.jpg';
        options.mimeType = 'image/jpeg';
        options.chunkedMode = false;
        options.headers = {
            Connection: 'close'
        };
        var params = {
            client_id: CONFIGURATION.TOKEN.CLIENT_ID,
            client_token: CONFIGURATION.TOKEN.CLIENT_TOKEN,
            user_id: this.userData.id,
            user_type: this.userData.user_type
        };

        options.params = params;
        var ft = new FileTransfer();
        this._utilitiesService.showLoading();
        console.log(CONFIGURATION.apiEndpoint + URL);
        console.log(params);
        ft.upload(imageUrl, CONFIGURATION.apiEndpoint + URL, response => {
            console.log(response);
            let res = JSON.parse(response.response);
            navigator.camera.cleanup(success => {
                console.log('success');
            }, error => {
                console.log('fail');
            });
            this._utilitiesService.hideLoading().then(resp => {
                if (res.status === 'OK') {
                    this.userData.profile_picture = res.data.imageUrl;

                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', res.data.message);
                }
            })

        }, error => {
            this._utilitiesService.hideLoading().then(resp => {
                console.log(JSON.stringify(error));
                // error.code == FileTransferError.ABORT_ERR
                alert('An error has occurred: Code = ' + error.code);
                console.log('upload error source ' + error.source);
                console.log('upload error target ' + error.target);
            });

        }, options, true);

    }

}