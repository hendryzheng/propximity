import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

import { UtilitiesService } from './utilities.service';
import { CONFIGURATION } from './config.service';
import { Platform } from '@ionic/angular';

declare var navigator: any;
declare var FileTransfer: any;
declare var FileUploadOptions: any;
declare var cordova: any;

@Injectable()
export class PropertyService {

    
    constructor(private _httpService: HttpService,
        public platform: Platform,
        private _utilitiesService: UtilitiesService) {
        // var userData = localStorage.getItem('userData');
        // console.log(userData);
    }

    public selectedPropertyType = '';
    public searchPropertyList: any = [];
    public availableDateTimeViewing: any = [];
    public selectedListing: any = [];
    public listing: any = [];
    public myListing: any = [];
    public account_detail: any = [];
    public selectedPackage: any = [];
    public selectedAccountTopupPurchase: any = [];
    public highlights: any = [];
    public tagAgents: any = [];
    public homeSearch: any = {
        location: '',
        location_display: ''
    }

    public districts = {
        'D01' : 'District 01 - Raffles Place, Marina, Cecil',
        'D02' : 'District 02 - Tanjong Pagar, Chinatown',
        'D03' : 'District 03 - Tiong Bahru, Alexandra, Queenstown',
        'D04' : 'District 04 - Mount Faber, Telok Blangah, Harbourfront',
        'D05' : 'District 05 - Buona Vista, Pasir Panjang, Clementi',
        'D06' : 'District 06 - Clarke Quay, City Hall',
        'D07' : 'District 07 - Bugis, Beach Road, Golden Mile',
        'D08' : 'District 08 - Little India, Farrer Park',
        'D09' : 'District 09 - Orchard Road, River Valley',
        'D10' : 'District 10 - Bukit Timah, Holland, Balmoral',
        'D11' : 'District 11 - Novena, Newton, Thomson',
        'D12' : 'District 12 - Toa Payoh, Serangoon, Balestier',
        'D13' : 'District 13 - Macpherson, Braddell',
        'D14' : 'District 14 - Geylang, Paya Lebar, Sims',
        'D15' : 'District 15 - Joo Chiat, Marina Parade, Katong',
        'D16' : 'District 16 - Bedok, Upper East Coast, Siglap',
        'D17' : 'District 17 - Changi, Flora, Loyang',
        'D18' : 'District 18 - Tampines, Pasir Ris',
        'D19' : 'District 19 - Punggol, Sengkang, Serangoon Gardens',
        'D20' : 'District 20 - Ang Mo Kio, Bishan, Thomson',
        'D21' : 'District 21 - Upper Bukit Timah, Ulu Pandan, Clementi Park',
        'D22' : 'District 22 - Boon Lay, Jurong, Tuas',
        'D23' : 'District 23 - Choa Chu Kang, Diary Farm, Hillview, Bukit Panjang, Bukit Batok',
        'D24' : 'District 24 - Kranji, Lim Chu Kang, Tengah',
        'D25' : 'District 25 - Woodlands, Admiralty',
        'D26' : 'District 26 - Upper Thomson, Mandai',
        'D27' : 'District 27 - Sembawang, Yishun, Admiralty',
        'D28' : 'District 28 - Yio Chu Kang, Seletar'
    };

    public hdbStates = {
        'Ang Mo Kio': 'Ang Mo Kio',
        'Bedok': 'Bedok',
        'Bishan': 'Bishan',
        'Bukit Batok': 'Bukit Batok',
        'Bukit Merah': 'Bukit Merah',
        'Bukit Panjang': 'Bukit Panjang',
        'Bukit Timah': 'Bukit Timah',
        'Central Area': 'Central Area',
        'Choa Chu Kang': 'Choa Chu Kang',
        'Clementi': 'Clementi',
        'Geylang': 'Geylang',
        'Hougang': 'Hougang',
        'Jurong East': 'Jurong East',
        'Jurong West': 'Jurong West',
        'Kallang/Whampoa': 'Kallang/Whampoa',
        'Lim Chu Kang': 'Lim Chu Kang',
        'Marine Parade': 'Marine Parade',
        'Pasir Ris': 'Pasir Ris',
        'Punggol': 'Punggol',
        'Queenstown': 'Queenstown',
        'Sembawang': 'Sembawang',
        'Sengkang': 'Sengkang',
        'Serangoon': 'Serangoon',
        'Simei': 'Simei',
        'Tampines': 'Tampines',
        'Toa Payoh': 'Toa Payoh',
        'Woodlands': 'Woodlands',
        'Yio Chu Kang': 'Yio Chu Kang',
        'Yishun': 'Yishun'
    };

    public listing_categories = [
        {
            key: 'residential',
            value: 'Residential',
            main_categories: [
                {
                    key: 'hdb',
                    value: 'HDB',
                    sub_categories: [
                        {
                            key: 'hdb_2r',
                            value: 'HDB 2 rooms',
                            checked: false
                        },
                        {
                            key: 'hdb_3r',
                            value: 'HDB 3 rooms',
                            checked: false
                        },
                        {
                            key: 'hdb_4r',
                            value: 'HDB 4 rooms',
                            checked: false
                        },
                        {
                            key: 'hdb_5r',
                            value: 'HDB 5 rooms',
                            checked: false
                        },
                        {
                            key: 'hdb_6r',
                            value: 'HDB 6 rooms',
                            checked: false
                        },
                        {
                            key: 'hdb_executive',
                            value: 'HDB Executive',
                            checked: false
                        }
                    ]
                },
                {
                    key: 'condo',
                    value: 'Condo',
                    sub_categories : [
                        {
                            key: 'generic_condo',
                            value: 'Generic Condo',
                            checked: false
                        },
                        {
                            key: 'condo_apartment',
                            value: 'Apartment',
                            checked: false
                        },
                        {
                            key: 'walkup',
                            value: 'Walk-up',
                            checked: false
                        }
                    ]
                },
                {
                    key: 'landed',
                    value: 'Landed',
                    sub_categories : [
                        {
                            key: 'terraced_house',
                            value: 'Terraced House',
                            checked: false
                        },
                        {
                            key: 'corner_terrace',
                            value: 'Corner Terrace',
                            checked: false
                        },
                        {
                            key: 'semi_detached',
                            value: 'Semi-detached house',
                            checked: false
                        },
                        {
                            key: 'bungalow',
                            value: 'Bungalow / Detached House',
                            checked: false
                        },
                        {
                            key: 'goodclass_bungalow',
                            value: 'Good Class Bungalow',
                            checked: false
                        },
                        {
                            key: 'shophouse',
                            value: 'Shophouse',
                            checked: false
                        },
                        {
                            key: 'conservationhouse',
                            value: 'Conservation House',
                            checked: false
                        },
                        {
                            key: 'townhouse',
                            value: 'Townhouse',
                            checked: false
                        },
                        {
                            key: 'clusterhouse',
                            value: 'Cluster House',
                            checked: false
                        },
                        {
                            key: 'landonly',
                            value: 'Land Only',
                            checked: false
                        },
                    ]
                }
            ]
        },
        {
            key: 'commercial',
            value: 'Commercial',
            main_categories : [
                {
                    key: 'retail',
                    value: 'Retail'
                },
                {
                    key: 'office',
                    value: 'Office'
                },
            ]
        },
        {
            key: 'industrial',
            value: 'Industrial',
            main_categories : [
                {
                    key: 'general_industrial',
                    value: 'General Industrial'
                },
                {
                    key: 'warehouse',
                    value: 'Warehouse'
                },
                {
                    key: 'showroom',
                    value: 'showroom'
                },
                {
                    key: 'dormitory',
                    value: 'dormitory'
                },
                {
                    key: 'ebiz',
                    value: 'E-Business'
                },
                {
                    key: 'fnb',
                    value: 'F&B / Canteen'
                },
                {
                    key: 'childcare',
                    value: 'Childcare'
                },
                {
                    key: 'industrial_office',
                    value: 'Industrial Office'
                },
                {
                    key: 'shop',
                    value: 'Shop'
                },
                {
                    key: 'factory',
                    value: 'Factory'
                },
            ]
        },
        {
            key: 'land',
            value: 'Land',
            main_categories : [
                {
                    key: 'general_land',
                    value: 'General Land'
                },
                {
                    key: 'land_w_building',
                    value: 'Land with Building'
                },
            ]
        }
    ]

    searchPropertyName(keyword, callback: ()=>void){
        // this._utilitiesService.showLoading();
        let url = 'https://developers.onemap.sg/commonapi/search?searchVal='+keyword+'&returnGeom=Y&getAddrDetails=Y&pageNum=1';
        this._httpService.getRequestPlain(url).subscribe(response => {
            setTimeout(()=>{
                if(response.results.length > 0){
                    for(var i=0;i<response.results.length;i++){
                        if(response.results[i].POSTAL !== 'NIL'){
                            this.searchPropertyList.push(response.results[i]);
                        }
                    }
                }
            },500);
            
            callback();

        },error => {
            // this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    getListing(params, callback: ()=>void){
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.getListing, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this.listing = response.data.listing
                        callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    getHighlights(callback: ()=>void){
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.getHighlights, {}).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this.highlights = response.data
                        callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }
    sendViewingRequest(params, callback: ()=>void){
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.sendViewingRequest, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                        callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }
    

    getMyListing(params, callback: ()=>void){
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.myListing, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this.myListing = response.data.listing;
                    this.account_detail = response.data.account;
                        callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    addListing(params, callback: ()=>void){
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.addListing, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    // this._utilitiesService.alertMessage('Success !', response.message).then(resolve => {
                        callback();
                    // });
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    deleteListing(params, callback: ()=>void){
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.deleteListing, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    // this._utilitiesService.alertMessage('Success !', response.message).then(resolve => {
                        callback();
                    // });
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    updateListing(params, callback: ()=>void){
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.updateListing, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this.myListing = response.data;
                    // this._utilitiesService.alertMessage('Success !', response.message).then(resolve => {
                        callback();
                    // });
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    createTopupPurchase(params, callback: ()=>void){
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.createTopupPurchase, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this.selectedAccountTopupPurchase = response.data;
                    // this._utilitiesService.alertMessage('Success !', response.message).then(resolve => {
                        callback();
                    // });
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    stripeCharge(params, callback: ()=>void){
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.stripeCharge, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    // this._utilitiesService.alertMessage('Success !', response.message).then(resolve => {
                        callback();
                    // });
                } else {
                    this._utilitiesService.alertMessage('Payment Unsuccessful', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    getTagAgents(params, callback: ()=>void){
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.getTagAgents, params).subscribe(response => {

            // this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    // this._utilitiesService.alertMessage('Success !', response.message).then(resolve => {
                        this.tagAgents = response.data;
                        callback();
                    // });
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            // });

        },error => {
            // this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    createTagAgents(params, callback: ()=>void){
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.addTagAgents, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    // this._utilitiesService.alertMessage('Success !', response.message).then(resolve => {
                        callback();
                    // });
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }
    

}