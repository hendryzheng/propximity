import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HomeSub1Page } from './home-sub1.page';
import { LocationModule } from '../location/location.module';

const routes: Routes = [
  {
    path: '',
    component: HomeSub1Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    LocationModule
  ],
  declarations: [HomeSub1Page]
})
export class HomeSub1PageModule {}
