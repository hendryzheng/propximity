import { Component, OnInit, NgZone } from '@angular/core';
import { Location } from '@angular/common';
import { PropertyService } from '../services/property.service';
import { ModalController } from '@ionic/angular';
import { LocationComponent } from '../location/location.component';

@Component({
  selector: 'app-home-sub1',
  templateUrl: './home-sub1.page.html',
  styleUrls: ['./home-sub1.page.scss'],
})
export class HomeSub1Page implements OnInit {

  constructor(private location: Location,
              private _propertyService: PropertyService,
              private zone: NgZone,
              private modalController: ModalController) { }

  ngOnInit() {
  }

  goBack() {
    this.location.back();
  }

  async presentModal(type) {
    
    this._propertyService.selectedPropertyType = type;
    const modal = await this.modalController.create({
      component: LocationComponent
    });
    modal.onDidDismiss()
      .then((dataResp: any) => {
        console.log(dataResp);
        let data = dataResp.data;
        this.zone.run(() => {
          this._propertyService.homeSearch.location = data.key;
          this._propertyService.homeSearch.location_display = data.value;
          if(typeof data.key !== 'undefined' && data.key !== null && data.key !== ''){
            this.goBack();
          }
        })
      });
    return await modal.present();
  }

}
