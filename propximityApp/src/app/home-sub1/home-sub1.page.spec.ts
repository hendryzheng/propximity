import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeSub1Page } from './home-sub1.page';

describe('HomeSub1Page', () => {
  let component: HomeSub1Page;
  let fixture: ComponentFixture<HomeSub1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeSub1Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeSub1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
