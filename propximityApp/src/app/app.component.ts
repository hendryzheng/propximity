import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AccountService } from './services/account.service';

declare var window: any;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(
    private platform: Platform,
    private _accountService: AccountService,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  ngOnInit(){
    this._accountService.checkUserSession();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this._accountService.checkUserSession();
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);
      this.statusBar.backgroundColorByHexString('#EEEEEE');
      this.initFirebase();

    });
  }

  private initFirebase(){
    if (typeof window.FirebasePlugin !== 'undefined') {
      document.addEventListener('deviceready', () => {

        if (this.platform.is('ios')) {
          window.FirebasePlugin.grantPermission();
          window.FirebasePlugin.getToken(token => {
            // save this server-side and use it to push notifications to this device
            console.log(token);
            if (token !== null) {
              this._accountService.token = token;
              this._accountService.registerToken();
            }
          }, error => {
            console.error(error);
          });
          window.FirebasePlugin.onTokenRefresh(token => {
            // save this server-side and use it to push notifications to this device
            console.log(token);
            if (token !== null) {
              this._accountService.token = token;
              this._accountService.registerToken();
            }

          }, error => {
            console.error(error);
          });

        } else {
          window.FirebasePlugin.getToken(token => {
            // save this server-side and use it to push notifications to this device
            console.log(token);
            if (token !== null) {
              this._accountService.token = token;
              this._accountService.registerToken();
            }
          }, error => {
            console.error(error);
          });
          window.FirebasePlugin.onTokenRefresh(token => {
            // save this server-side and use it to push notifications to this device
            console.log(token);
            if (token !== null) {
              this._accountService.token = token;
              this._accountService.registerToken();
            }

          }, error => {
            console.error(error);
          });
        }
        // window.FirebasePlugin.onNotificationOpen(payload => {
        //   console.log(payload);
        //   console.log(payload.aps);
        //   console.log(payload.aps.alert);
        //   this.router.navigateByUrl('/notifications');
        //   // this._utilitiesService.alertMessageCallback('Notification received', payload.aps.alert.title, () => {
        //   //   // this.nav.setRoot(TabsPage);
        //   // })
        // }, (error) => {
        //   console.error(error);
        // });



      }, false);

    }
  }
}
