import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditBalanceTopupPage } from './credit-balance-topup.page';

describe('CreditBalanceTopupPage', () => {
  let component: CreditBalanceTopupPage;
  let fixture: ComponentFixture<CreditBalanceTopupPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditBalanceTopupPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditBalanceTopupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
