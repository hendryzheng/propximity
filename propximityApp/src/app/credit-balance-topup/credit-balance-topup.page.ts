import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { UtilitiesService } from '../services/utilities.service';
import { HttpService } from '../services/http.service';
import { CONFIGURATION } from '../services/config.service';
import { parse } from 'querystring';
import { AlertController } from '@ionic/angular';
import { PropertyService } from '../services/property.service';
import { Router } from '@angular/router';
import { AccountService } from '../services/account.service';

declare var StripeCheckout;


@Component({
  selector: 'app-credit-balance-topup',
  templateUrl: './credit-balance-topup.page.html',
  styleUrls: ['./credit-balance-topup.page.scss'],
})
export class CreditBalanceTopupPage implements OnInit {

  constructor(private location: Location,
    private _utilitiesService: UtilitiesService,
    private router: Router,
    private _accountService: AccountService,
    private _propertyService: PropertyService,
    private _httpService: HttpService) { }

  packages: any = [];
  handler: any = null;

  ngOnInit() {
  }

  ionViewWillEnter() {
    this._accountService.checkUserSession();
    this.fetch();
  }

  select(item){
    this._propertyService.selectedPackage = item.value;
    let params = {
      'AccountTopupPurchase[account_id]': this._accountService.userData.id,
      'AccountTopupPurchase[topup_package_id]': item.value.id,
      'AccountTopupPurchase[topup_amount]': item.value.package_amount_price,
      'AccountTopupPurchase[credit_amount]': item.value.credit_amount,
      'AccountTopupPurchase[payment_status]': 0
    };
    this._propertyService.createTopupPurchase(params, ()=>{
      this.router.navigateByUrl('/payment-details');
    });
  }

  fetch() {
    this._utilitiesService.showLoading();
    let param = [];
    this._httpService.postRequest(CONFIGURATION.URL.getTopupPackage, param).subscribe(response => {

      this._utilitiesService.hideLoading().then(resp => {
        if (response.status === 'OK') {
          this.packages = response.data;
        } else {
          this._utilitiesService.alertMessage('Oops! Something happened', response.message);
        }
      });

    }, error => {
      this._utilitiesService.hideLoading();
      console.log(error);
    });
  }

  key(key){
    return parseInt(key)+1;
  }

  goBack() {
    this.location.back();
  }

}
