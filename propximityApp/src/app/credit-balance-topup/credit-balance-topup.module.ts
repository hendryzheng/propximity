import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CreditBalanceTopupPage } from './credit-balance-topup.page';

const routes: Routes = [
  {
    path: '',
    component: CreditBalanceTopupPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CreditBalanceTopupPage]
})
export class CreditBalanceTopupPageModule {}
