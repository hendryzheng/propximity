import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListingDateTimeInputPage } from './listing-date-time-input.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ],
  entryComponents: [ListingDateTimeInputPage],
  declarations: [ListingDateTimeInputPage]
})
export class ListingDateTimeInputModule {}
