import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListingDateTimeInputPage } from './listing-date-time-input.page';

describe('ListingDateTimeInputPage', () => {
  let component: ListingDateTimeInputPage;
  let fixture: ComponentFixture<ListingDateTimeInputPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListingDateTimeInputPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingDateTimeInputPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
