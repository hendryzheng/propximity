import { Component, OnInit } from '@angular/core';

import * as moment from 'moment';
import { UtilitiesService } from '../services/utilities.service';
import { ModalController } from '@ionic/angular';
import { PropertyService } from '../services/property.service';
import { DatePicker } from '@ionic-native/date-picker/ngx';

@Component({
  selector: 'app-listing-date-time-input',
  templateUrl: './listing-date-time-input.page.html',
  styleUrls: ['./listing-date-time-input.page.scss'],
})
export class ListingDateTimeInputPage implements OnInit {

  startDate: any = moment().format('YYYY-MM-DD HH:mm');
  startTime: any = '08:00';

  endDate: any = moment(this.startDate, 'YYYY-MM-DD').add(1, 'days').format('YYYY-MM-DD') ;
  endTime: any = '21:00';
  minDate: string = new Date().toISOString();
  duration: any = '1';

  constructor(private _utilitiesService: UtilitiesService,
              private _propertyService: PropertyService,
              private datePicker: DatePicker,
              private _modalController: ModalController) { }

  ngOnInit() {
  }

  add(){
    let data = {
      startDateTime: this.startDate,
      endDateTime: this.endDate,
      display: moment(this.startDate, 'YYYY-MM-DD HH:mm').format('DD MMM YYYY HH:mm')+ ' to ' +moment(this.endDate, 'YYYY-MM-DD HH:mm').format('HH:mm')
    };
    this._propertyService.availableDateTimeViewing.push(data);
    this._modalController.dismiss(data);
  }

  updateEndTime($ev){
    this.endDate = moment(this.startDate, 'YYYY-MM-DD HH:mm').add(this.duration, 'hours').format('YYYY-MM-DD HH:mm') ;
  }

  openDatepicker(){
    this.datePicker.show({
      date: new Date(),
      mode: 'datetime',
      minDate: this.minDate,
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {

        console.log('Got date: ', date);
        this.startDate = moment(date).format('YYYY-MM-DD HH:mm');
        this.endDate = moment(this.startDate, 'YYYY-MM-DD HH:mm').add(this.duration, 'hours').format('YYYY-MM-DD HH:mm') ;
        console.log(this.startDate);
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  close(){
    this._modalController.dismiss();
  }

}
