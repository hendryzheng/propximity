import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  { path: 'notifications', loadChildren: './notifications/notifications.module#NotificationsPageModule' },
  { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'search-listing', loadChildren: './search-listing/search-listing.module#SearchListingPageModule' },
  { path: 'create-post', loadChildren: './create-post/create-post.module#CreatePostPageModule' },
  { path: 'send-request', loadChildren: './send-request/send-request.module#SendRequestPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'forgot-password', loadChildren: './forgot-password/forgot-password.module#ForgotPasswordPageModule' },
  { path: 'registration', loadChildren: './registration/registration.module#RegistrationPageModule' },
  { path: 'filter-page', loadChildren: './filter-page/filter-page.module#FilterPagePageModule' },
  { path: 'property-viewing-appointment', loadChildren: './property-viewing-appointment/property-viewing-appointment.module#PropertyViewingAppointmentPageModule' },
  { path: 'credit-balance-topup', loadChildren: './credit-balance-topup/credit-balance-topup.module#CreditBalanceTopupPageModule' },
  { path: 'payment-process', loadChildren: './payment-process/payment-process.module#PaymentProcessPageModule' },
  { path: 'payment-details', loadChildren: './payment-details/payment-details.module#PaymentDetailsPageModule' },
  { path: 'edit-post', loadChildren: './edit-post/edit-post.module#EditPostPageModule' },
  { path: 'viewing-request-list', loadChildren: './viewing-request-list/viewing-request-list.module#ViewingRequestListPageModule' },
  { path: 'home-sub1', loadChildren: './home-sub1/home-sub1.module#HomeSub1PageModule' },
  { path: 'edit-profile', loadChildren: './edit-profile/edit-profile.module#EditProfilePageModule' },
  { path: 'my-listing', loadChildren: './my-listing/my-listing.module#MyListingPageModule' },
  { path: 'top-up-history', loadChildren: './top-up-history/top-up-history.module#TopUpHistoryPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
