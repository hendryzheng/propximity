import { Component, OnInit, NgZone } from '@angular/core';
import { AccountService } from '../services/account.service';
import { PropertyService } from '../services/property.service';

import * as moment from 'moment';
import { UtilitiesService } from '../services/utilities.service';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { SearchpropertyComponent } from '../searchproperty/searchproperty.component';
import { DatePicker } from '@ionic-native/date-picker/ngx';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  startDate: any = moment().format('ddd, DD MMM YY');
  startTime: any = '08:00';

  endDate: any = moment(this.startDate, 'YYYY-MM-DD').add(1, 'days').format('YYYY-MM-DD') ;
  endTime: any = '21:00';

  toggleFilter: boolean = true;

  form = {
    listing_type: 'sale',
    property_type: '',
    property_name: '',
    address: '',
    location: '',
    postal: '',
    lat: '',
    lng: '',
    date_filter: this.startDate,
    start_time: this.startTime,
    end_time: this.endTime,
    all_day: false,
  };

  search_display: any = '';
  search_result: any = false;
  date_display: any = this.startDate;
  fullname: any = '';

  constructor(private _accountService: AccountService,
              private _propertyService: PropertyService,
              private datePicker: DatePicker,
              private modalController: ModalController,
              private router: Router,
              private zone: NgZone,
              private _utilitiesService: UtilitiesService) { }

  ngOnInit() {
  }

  toggle(){
    this.toggleFilter = !this.toggleFilter;
  }

  ionViewWillEnter(){
    this._accountService.checkUserSession();
    if(this._accountService.isUserSessionAlive()){
      this.fullname = this._accountService.userData.fullname;
    }
    let param = {
      listing_type: 'sale',
      property_type: '',
      property_name: '',
      address: '',
      location: '',
      postal: '',
      lat: '',
      lng: '',
      date_filter: '',
      start_time: '',
      end_time: '',
      all_day: false,
    };
    this._propertyService.getListing(param, ()=>{
      this.search_display = this._propertyService.homeSearch.location_display;
    });
  }

  openDatepicker(){
    this.datePicker.show({
      date: new Date(),
      mode: 'datetime',
      allowOldDates: false,
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {

        console.log('Got date: ', date);
        this.date_display = moment(date).format('ddd, DD MMM YYYY');
        this.form.start_time = moment(date).format('HH:mm');
        this.form.date_filter = date;
        console.log(this.startDate);
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  getLocationDisplay(){
    return this._propertyService.homeSearch.location_display;
  }

  clearFilter(){
    this.search_result = false;
    this._propertyService.homeSearch.location = '';
    this._propertyService.homeSearch.location_display = '';
    this.form.date_filter = this.startDate;
    let param = {
      listing_type: 'sale',
      property_type: '',
      property_name: '',
      address: '',
      location: '',
      postal: '',
      lat: '',
      lng: '',
      date_filter: '',
      start_time: '',
      end_time: '',
      all_day: false,
    };
    this._propertyService.getListing(param, ()=>{
      this.search_display = this._propertyService.homeSearch.location_display;
    });
  }

  filterSubmit(){
    let params = this.form;
    var date = new Date(this.form.date_filter);
    var year = date.getFullYear();
    var month: any = date.getMonth()+1;
    var dt: any = date.getDate();

    if (dt < 10) {
      dt = '0' + dt;
    }
    if (month < 10) {
      month = '0' + month;
    }

    console.log(year+'-' + month + '-'+dt);
    params['date_filter'] = year+'-' + month + '-'+dt;
    params['location'] = this._propertyService.homeSearch.location;
    params['listing_type'] = this.form.listing_type;
    this._propertyService.getListing(this.form, ()=>{
      this.search_result = true;
    });
  }

  toggleListingType(type){
    this.form.listing_type = type;
  }

  isLoggedIn(){
    return this._accountService.isUserSessionAlive();
  }

  getUserData(){
    return this._accountService.userData;
  }

  getListing(){
    return this._propertyService.listing;
  }

  openBrowser(title, url){
    this._utilitiesService.openBrowser(title, url);
  }

  formatDate(dt){
    return moment(dt, 'YYYY-MM-DD HH:mm:ss').format('DD MMM');
  }

  formatTime(dt){
    return moment(dt, 'YYYY-MM-DD HH:mm:ss').format('hh a');
  }

  getListingType(){
    return this._propertyService.listing_categories;
  }

  sendRequest(item){
    this._propertyService.selectedListing = item;
    this.router.navigateByUrl('/send-request');
  }

  refresh(){
    let param = {
      listing_type: 'sale',
      property_type: '',
      property_name: '',
      address: '',
      location: '',
      postal: '',
      lat: '',
      lng: '',
      date_filter: '',
      start_time: '',
      end_time: '',
      all_day: false,
    };
    this._propertyService.getListing(param, ()=>{
      this.search_display = this._propertyService.homeSearch.location_display;
    });
  }

  async presentModalProperty() {
    const modal = await this.modalController.create({
      component: SearchpropertyComponent
    });
    modal.onDidDismiss()
      .then((dataResp: any) => {
        console.log(dataResp);
        let data = dataResp.data;
        this.zone.run(() => {
          this.form.property_name = data.BUILDING;
          this.form.address = data.ADDRESS;
          if(data.POSTAL !== 'NIL'){
            this.form.postal = data.POSTAL;
          }
          this.form.lat = data.LATITUDE;
          this.form.lng = data.LONGITUDE;
          console.log(this.form);
        })
      });
    return await modal.present();
  }

}
