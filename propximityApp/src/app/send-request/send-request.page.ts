import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { PropertyService } from '../services/property.service';
import { UtilitiesService } from '../services/utilities.service';

import * as moment from 'moment';

declare var cordova: any;

@Component({
  selector: 'app-send-request',
  templateUrl: './send-request.page.html',
  styleUrls: ['./send-request.page.scss'],
})
export class SendRequestPage implements OnInit {

  constructor(private location: Location,
              private _utilitiesService: UtilitiesService,
              private _propertyService: PropertyService) { }

  form = {
    fullname: '',
    email: '',
    mobile: '',
    message: ''
  }      

  ngOnInit() {
  }

  goBack(){
    this.location.back();
  }

  getSelectedListing(){
    return this._propertyService.selectedListing;
  }


  openBrowser(title, url){
    this._utilitiesService.openBrowser(title, url);
  }

  formatDate(dt){
    return moment(dt, 'YYYY-MM-DD HH:mm:ss').format('DD MMM');
  }

  formatTime(dt){
    return moment(dt, 'YYYY-MM-DD HH:mm:ss').format('hh a');
  }

  getListingType(){
    return this._propertyService.listing_categories;
  }

  submit(){
    if(this.validateForm()){
      let params = {
        'SendViewRequest[name]': this.form.fullname,
        'SendViewRequest[email]': this.form.email,
        'SendViewRequest[mobile]': this.form.mobile,
        'SendViewRequest[message]': this.form.message,
        'SendViewRequest[listing_id]': this._propertyService.selectedListing.id
      }
      this._propertyService.sendViewingRequest(params, ()=>{
        let phone = this._propertyService.selectedListing.account.mobile;
        var url =  'https://api.whatsapp.com/send?phone='+this._utilitiesService.formatWhatsappNo(phone);

        this._utilitiesService.alertMessageCallback('Success','Your viewing request has been sent to the respective listing owner. Please come at the stated viewing time', ()=>{
          this.form.fullname = '';
          this.form.email = '';
          this.form.mobile = '';
          this.form.message = '';
          var ref = cordova.InAppBrowser.open(url, '_system', 'location=yes');

        });
      });
    }
  }

  private validateForm() {
    var validate = true;
    console.log(this.form);
    if (this._utilitiesService.isEmpty(this.form.fullname)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.mobile)) {
      validate = false;
    }
    // if (this._utilitiesService.isEmpty(this.form.email)) {
    //   validate = false;
    // }
    if (this._utilitiesService.isEmpty(this.form.message)) {
      validate = false;
    }
    
    if (!validate) {
      this._utilitiesService.alertMessage('Validation Error', 'Please fill up all details');
      return false;
    }
    console.log(validate);
    return validate;
  }

}
