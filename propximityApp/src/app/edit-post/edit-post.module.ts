import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EditPostPage } from './edit-post.page';

import { LocationComponent } from '../location/location.component';
import { SearchpropertyComponent } from '../searchproperty/searchproperty.component';
import { ListingDateTimeInputPage } from '../listing-date-time-input/listing-date-time-input.page';
import { SearchpropertyModule } from '../searchproperty/searchproperty.module';
import { LocationModule } from '../location/location.module';
import { ListingDateTimeInputModule } from '../listing-date-time-input/listing-date-time-input.module';

const routes: Routes = [
  {
    path: '',
    component: EditPostPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchpropertyModule,
    LocationModule,
    ListingDateTimeInputModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EditPostPage]
})
export class EditPostPageModule {}
