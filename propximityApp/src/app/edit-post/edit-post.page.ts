import { Component, OnInit, NgZone } from '@angular/core';
import { PropertyService } from '../services/property.service';
import { ModalController } from '@ionic/angular';
import { LocationComponent } from '../location/location.component';
import { UtilitiesService } from '../services/utilities.service';
import { SearchpropertyComponent } from '../searchproperty/searchproperty.component';
import { ListingDateTimeInputPage } from '../listing-date-time-input/listing-date-time-input.page';
import { AccountService } from '../services/account.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { count } from 'rxjs/operators';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.page.html',
  styleUrls: ['./edit-post.page.scss'],
})
export class EditPostPage implements OnInit {

  form = {
    listing_type: '',
    property_type: '',
    hdb_type: '',
    property_name: '',
    blk_no: '',
    road_name: '',
    building: '',
    address: '',
    postal: '',
    lat: '',
    lng: '',
    highlight: '',
    location: '',
    floor_area: '',
    number_of_rooms: '',
    url: '',
    vacant_type: '',
    asking_price: '',
    com_agent_profile: '',
    available_date_viewing: '',
    account_id: 0,
    expected_close: '',
  };
  label_display: any = 'Property Name';
  vacant = [
    { val: 'Vacant', isChecked: false },
    { val: 'Tenant', isChecked: false },
    { val: 'Owner Stay', isChecked: false }
  ];
  highlights_arr = [];

  inAppWindow: any = null;
  highlights: any = [];

  location_display: '';

  constructor(private _propertyService: PropertyService,
              private _utilitiesService: UtilitiesService,
              private _accountService: AccountService,
              private modalController: ModalController,
              private router: Router,
              private zone: NgZone,
              private location: Location
              ) { }

  ngOnInit() {
  }

  goBack(){
    this.location.back();
  }

  ionViewWillEnter(){
    this._accountService.checkUserSession();
    this._propertyService.getHighlights(()=>{
      this.highlights = this._propertyService.highlights;
    })
    this.form.account_id = this._accountService.userData.id;
    this.form = {
      listing_type: this._propertyService.selectedListing.listing_type,
      property_type: this._propertyService.selectedListing.property_type,
      hdb_type: this._propertyService.selectedListing.hdb_type,
      property_name: this._propertyService.selectedListing.property_name,
      blk_no: this._propertyService.selectedListing.blk_no,
      road_name: this._propertyService.selectedListing.road_name,
      building: this._propertyService.selectedListing.building,
      address: this._propertyService.selectedListing.address,
      postal: this._propertyService.selectedListing.postal,
      lat: this._propertyService.selectedListing.lat,
      lng: this._propertyService.selectedListing.lng,
      highlight: this._propertyService.selectedListing.highlight,
      location: this._propertyService.selectedListing.location,
      floor_area: this._propertyService.selectedListing.floor_area,
      number_of_rooms: this._propertyService.selectedListing.no_of_rooms,
      url: this._propertyService.selectedListing.url,
      vacant_type: this._propertyService.selectedListing.vacant_type,
      asking_price: this._propertyService.selectedListing.asking_price,
      com_agent_profile:this._propertyService.selectedListing.com_agent_profile,
      available_date_viewing: '',
      account_id: this._accountService.userData.id,
      expected_close: this._propertyService.selectedListing.expected_close
    };
    if(this._propertyService.selectedListing.location.length == 3){
      this.location_display = this._propertyService.districts[this._propertyService.selectedListing.location];
    }else{
      this.location_display = this._propertyService.selectedListing.location;
    }
    if(this.form.vacant_type !== ''){
      var split = this.form.vacant_type.split(',');
      if(split.length > 1){
        for(var i=0;i<split.length;i++){
          for(var j=0;j<this.vacant.length;j++){
            if(this.vacant[j].val == split[i]){
              this.vacant[j].isChecked = true;
            }
          }
        }
      }
    }
  }

  getListingType(){
    return this._propertyService.listing_categories;
  }

  propertyTypeChange(ev){
    if(this.form.property_type == 'hdb'){
      this.label_display = 'HDB block and street name';
    }else{
      this.label_display = 'Property Name';
    }
  }

  getDistrict(){
    return this._propertyService.districts;
  }

  delete(){
    this.deleteAlert();
  }


  async deleteAlert() {
    const forgot = await this._utilitiesService.getAlertCtrl().create({
      header: 'Delete Confirmation',
      message: 'Are you sure you want to delete this listing? Once deleted, it cannot be undone.',
      
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes, I want to delete',
          handler: data => {
            console.log('Send clicked');
            let param = {
              id: this._propertyService.selectedListing.id
            }
            this._propertyService.deleteListing(param, () => {
              this._utilitiesService.alertMessageCallback('Success','Listing deleted successfully !', ()=>{
                this.goBack();
              });
            });
            // let toast = this.toastCtrl.create({
            //   message: 'Email was sended successfully',
            //   duration: 3000,
            //   position: 'top',
            //   cssClass: 'dark-trans',
            //   closeButtonText: 'OK',
            //   showCloseButton: true
            // });
            // toast.present();
          }
        }
      ]
    });
    forgot.present();
  }


  submit(){
    if(this.validateForm()){
      let formSubmission = {
        'Listing[listing_type]': this.form.listing_type,
        'Listing[property_type]': this.form.property_type,
        'Listing[property_name]': this.form.property_name,
        'Listing[hdb_type]': this.form.hdb_type,
        'Listing[blk_no]': this.form.blk_no,
        'Listing[road_name]': this.form.road_name,
        'Listing[building]': this.form.building,
        'Listing[address]': this.form.address,
        'Listing[postal]': this.form.postal,
        'Listing[lat]': this.form.lat,
        'Listing[lng]': this.form.lng,
        'Listing[highlight]': this.form.highlight,
        'Listing[location]': this.form.location,
        'Listing[floor_area]': this.form.floor_area,
        'Listing[no_of_rooms]': this.form.number_of_rooms,
        'Listing[url]': this.form.url,
        'Listing[vacant_type]': this.form.vacant_type,
        'Listing[asking_price]': this.form.asking_price,
        'Listing[com_agent_profile]': this.form.com_agent_profile,
        'Listing[account_id]': this.form.account_id,
        'Listing[expected_close]': this.form.expected_close,
        'available_date_viewing': JSON.stringify(this._propertyService.availableDateTimeViewing),
        'listing_id' : this._propertyService.selectedListing.id
      };
      this._propertyService.updateListing(formSubmission, ()=>{
        var msg = '';
        msg = 'Listing updated! All the best to you';
        this._utilitiesService.alertMessage('Success',msg);
        this.location.back();
      });
    }
    
  }

  private validateForm() {
    var validate = true;
    console.log(this.form);
    var vacant = '';
    for(var i=0;i<this.vacant.length;i++){
      if(this.vacant[i].isChecked){
        vacant += this.vacant[i].val+',';
      }
    }
    if(vacant !== ''){
      this.form.vacant_type = vacant.substring(0, vacant.length - 1);;
    }
    if (this._utilitiesService.isEmpty(this.form.property_name)) {
      validate = false;
      this._utilitiesService.alertMessage('Validation Error', 'Property name cannot be empty');
      return;
    }
    if (this._utilitiesService.isEmpty(this.form.address)) {
      validate = false;
      this._utilitiesService.alertMessage('Validation Error', 'Address cannot be empty');
      return;
    }

    // if (this._utilitiesService.isEmpty(this.form.highlight)) {
    //   validate = false;
    //   this._utilitiesService.alertMessage('Validation Error', 'Highlight cannot be empty');
    //   return;
    // }
    if (this._utilitiesService.isEmpty(this.form.listing_type)) {
      validate = false;
      this._utilitiesService.alertMessage('Validation Error', 'Listing type cannot be empty');
      return;
    }
    if (this._utilitiesService.isEmpty(this.form.property_type)) {
      validate = false;
      this._utilitiesService.alertMessage('Validation Error', 'Property type cannot be empty');
      return;
    }
    if (this._utilitiesService.isEmpty(this.form.location)) {
      validate = false;
      this._utilitiesService.alertMessage('Validation Error', 'Location cannot be empty');
      return;
    }
    if (this._utilitiesService.isEmpty(this.form.vacant_type)) {
      validate = false;
      this._utilitiesService.alertMessage('Validation Error', 'Vacant Type cannot be empty');
      return;
    }
    if (this._utilitiesService.isEmpty(this.form.asking_price)) {
      validate = false;
      this._utilitiesService.alertMessage('Validation Error', 'Asking Price cannot be empty');
      return;
    }
    // if (this._propertyService.availableDateTimeViewing.length < 1) {
    //   validate = false;
    //   this._utilitiesService.alertMessage('Validation Error', 'Available Date Time Viewing cannot be empty');
    //   return;
    // }

    if (!validate) {
      this._utilitiesService.alertMessage('Validation Error', 'Please fill up all details');
      return false;
    }
    console.log(validate);
    return validate;
  }

  async presentModal(){
    console.log(this.form);
    if(this.form.property_type == ''){
      this._utilitiesService.alertMessage('Property type empty','Please select property type');
      return;
    }
    this._propertyService.selectedPropertyType = this.form.property_type;
    const modal = await this.modalController.create({
      component: LocationComponent
    });
    modal.onDidDismiss()
      .then((dataResp: any) => {
        console.log(dataResp);
        let data = dataResp.data;
        this.zone.run(()=>{
          this.form.location = data.key;
          this.location_display = data.value;
        })
    });
    return await modal.present();
  }

  async presentModalProperty(){
    const modal = await this.modalController.create({
      component: SearchpropertyComponent
    });
    modal.onDidDismiss()
      .then((dataResp: any) => {
        console.log(dataResp);
        let data = dataResp.data;
        this.zone.run(()=>{
          this.form.property_name = data.BUILDING;
          this.form.blk_no = data.BLK_NO;
          this.form.address = data.ADDRESS;
          this.form.road_name = data.ROAD_NAME;
          this.form.building = data.BUILDING;
          this.form.postal = data.POSTAL;
          this.form.lat = data.LATITUDE;
          this.form.lng = data.LONGITUDE;
          console.log(this.form);
        })
    });
    return await modal.present();
  }

  async presentModalInputTime(){
    const modal = await this.modalController.create({
      component: ListingDateTimeInputPage
    });
    modal.onDidDismiss()
      .then((dataResp: any) => {

      });
      return await modal.present();
  }

  getAvailableTimeViewing(){
    return this._propertyService.availableDateTimeViewing;
  }

  remove(key){
    this._propertyService.availableDateTimeViewing.splice(key,1);
  }

}
