import { Component, OnInit } from '@angular/core';
import { AccountService } from '../services/account.service';
import { UtilitiesService } from '../services/utilities.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {

  form = {
    fullname: '',
    cea_no: '',
    agency: '',
    roles: '',
    email: '',
    password: '',
    confirm_password: '',
    mobile: ''
  }

  constructor(
    public location: Location,
    public _accountService: AccountService,
    public _utilitiesService: UtilitiesService
  ) { }

  ngOnInit() {
  }

  private callbackSuccess() {
    this._utilitiesService.hideLoading();
    this.form = { 
      fullname: '',
      cea_no: '',
      agency: '',
      roles: '',
      email: '',
      password: '',
      confirm_password: '',
      mobile: ''
    };
  }

  change(ev){
    console.log(ev);
  }


  toggleAgent(event) {
    console.log(event);
  }

  ngAfterViewInit() {
    // let tabs = document.querySelectorAll('.show-tabbar');
    // if (tabs !== null) {
    //   Object.keys(tabs).map((key) => {
    //     tabs[key].style.display = 'none';
    //   });
    // }
  }

  ionViewWillLeave() {
    // let tabs = document.querySelectorAll('.show-tabbar');
    // if (tabs !== null) {
    //   Object.keys(tabs).map((key) => {
    //     tabs[key].style.display = 'flex';
    //   });

    // }
  }


  submitRegister() {
    if (this.validateForm()) {
      let formSubmission = {
        'Account[fullname]': this.form.fullname,
        'Account[password]': this.form.password,
        'Account[email]': this.form.email,
        'Account[mobile]': this.form.mobile,
        'Account[cea_no]': this.form.cea_no,
        'Account[agency]': this.form.agency
      };
      this._accountService.register(formSubmission, () => {
        this.callbackSuccess();
      });
    }

  }

  private validateForm() {
    var validate = true;
    console.log(this.form);
    if (this._utilitiesService.isEmpty(this.form.fullname)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.roles)) {
      validate = false;
    }
    if(this.form.roles == 'agent'){
      if (this._utilitiesService.isEmpty(this.form.cea_no)) {
        validate = false;
      }
      if (this._utilitiesService.isEmpty(this.form.agency)) {
        validate = false;
      }
    }
    
    if (this._utilitiesService.isEmpty(this.form.email)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.mobile)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.password)) {
      validate = false;
    }
    if (this.form.password !== this.form.confirm_password) {
      this._utilitiesService.alertMessage('Validation Error', 'Password not equal with Confirm Password');
      return false;
    }
    if (!validate) {
      this._utilitiesService.alertMessage('Validation Error', 'Please fill up all details');
      return false;
    }
    console.log(validate);
    return validate;
  }

}
