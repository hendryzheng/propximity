<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="format-detection" content="telephone=no">

    <!-- favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- fontawesome -->
    <link rel="stylesheet" href="assets/fonts/font-awesome/css/font-awesome.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- Phillip Futures Styles -->
    <link rel="stylesheet" href="assets/styles/propximity.css">
    <title>Propximity</title>
  </head>
  <body>
    <div class="section header fixed-top">
      <div class="container">
        <nav class="navbar navbar-expand-xl navbar-light">
          <a class="navbar-brand" href="index.php">
            <img src="assets/images/logo-propximity.svg" alt="Propximity">
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="#">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">About</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Download App</a>
              </li>
              <li>
              <div class="link-account">
                <a class="btn btn-primary" href="#">Login</a>
              </div>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
