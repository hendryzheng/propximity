<?php include "base/header.php" ?>
<div class="container main-content">
  <div class="section-filter">
    <form>
      <div class="form-group">
        <label>Location</label>
        <div class="input">
          <input type="text" class="form-control" placeholder="Enter your location">
        </div>
      </div>
      <div class="form-group">
        <label>Housing Type</label>
        <div class="input">
          <select class="form-control">
            <option>Select Type</option>
            <option>Type 2</option>
            <option>Type 3</option>
            <option>Type 4</option>
            <option>Type 5</option>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label>Day</label>
        <div class="input">
          <input type="date" class="form-control" placeholder="23 / 7 / 2019">
        </div>
      </div>
      <div class="form-group form-multiple">
        <label>
          Time
          <div class="form-check">
            <input type="checkbox" class="form-check-input">
            <label class="form-check-label">All Day</label>
          </div>
        </label>
        <div class="input">
          <input type="date" class="form-control" placeholder="11 am">
          <span>To</span>
          <input type="date" class="form-control" placeholder="1 pm">
        </div>
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary">Find Properties</button>
      </div>
    </form>
  </div>
  <div class="section-results">
    <div class="section-listing">
      <div class="top">
        <div class="title">
          <h2>We've found your search</h2>
          <h5>Contact these agents for personalized viewings</h5>
        </div>
        <div class="view-all"><a href="#">View All</a></div>
      </div>
      <div class="items">
        <div class="item">
          <div class="product-details">
            <div class="label">
              <span class="badge badge-success">Tenanted</span>
              <span class="badge badge-warning">2% Comm</span>
            </div>
            <div class="description">
              <div class="title-price">
                <h2>Piermont Grand</h2>
                <div class="price">$799.19</div>
              </div>
              <div class="specification">
                <div class="item">
                  <span class="label">Bedrooms</span>
                  <span class="value">4</span>
                </div>
                <div class="item">
                  <span class="label">Bathrooms</span>
                  <span class="value">2</span>
                </div>
                <div class="item">
                  <span class="label">1248sqft</span>
                </div>
              </div>
              <div class="url">
                <span>URL: </span>
                <a href="">https://www.99.co/blog/singapore/a-home-above-allen-and-lorem-ipsum-dolor-sit-amet</a>
              </div>
              <div class="location">
                <div class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                <span>Marriot Hotel, 99 Irrawaddy Road, Novena Singapore, Singapore 329568</span>
              </div>
              <div class="available-viewing-time">
                <p>Available viewing time</p>
                <div class="item">
                  <span>Day</span>
                  <div class="value">Monday</div>
                </div>
                <div class="item">
                  <span>Date</span>
                  <div class="value">July 4 2019</div>
                </div>
                <div class="item">
                  <span>Time</span>
                  <div class="value">11 am - 1 pm</div>
                </div>
              </div>
              <div class="agent">
                <div class="agent-left">
                  <div class="item-avatar">
                    <div class="label">
                      <h2>Jennifer Ford</h2>
                      <p>Propximity Agents</p>
                    </div>
                  </div>
                </div>
                <div class="button">
                  <a href="#" class="btn btn-sm btn-primary">Contact Agent</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="product-details">
            <div class="label">
              <span class="badge badge-success">Tenanted</span>
              <span class="badge badge-warning">2% Comm</span>
            </div>
            <div class="description">
              <div class="title-price">
                <h2>Piermont Grand</h2>
                <div class="price">$799.19</div>
              </div>
              <div class="specification">
                <div class="item">
                  <span class="label">Bedrooms</span>
                  <span class="value">4</span>
                </div>
                <div class="item">
                  <span class="label">Bathrooms</span>
                  <span class="value">2</span>
                </div>
                <div class="item">
                  <span class="label">1248sqft</span>
                </div>
              </div>
              <div class="url">
                <span>URL: </span>
                <a href="">https://www.99.co/blog/singapore/a-home-above-allen-and-lorem-ipsum-dolor-sit-amet</a>
              </div>
              <div class="location">
                <div class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                <span>Marriot Hotel, 99 Irrawaddy Road, Novena Singapore, Singapore 329568</span>
              </div>
              <div class="available-viewing-time">
                <p>Available viewing time</p>
                <div class="item">
                  <span>Day</span>
                  <div class="value">Monday</div>
                </div>
                <div class="item">
                  <span>Date</span>
                  <div class="value">July 4 2019</div>
                </div>
                <div class="item">
                  <span>Time</span>
                  <div class="value">11 am - 1 pm</div>
                </div>
              </div>
              <div class="agent">
                <div class="agent-left">
                  <div class="item-avatar">
                    <div class="label">
                      <h2>Jennifer Ford</h2>
                      <p>Propximity Agents</p>
                    </div>
                  </div>
                </div>
                <div class="button">
                  <a href="#" class="btn btn-sm btn-primary">Contact Agent</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="product-details">
            <div class="label">
              <span class="badge badge-success">Tenanted</span>
              <span class="badge badge-warning">2% Comm</span>
            </div>
            <div class="description">
              <div class="title-price">
                <h2>Piermont Grand</h2>
                <div class="price">$799.19</div>
              </div>
              <div class="specification">
                <div class="item">
                  <span class="label">Bedrooms</span>
                  <span class="value">4</span>
                </div>
                <div class="item">
                  <span class="label">Bathrooms</span>
                  <span class="value">2</span>
                </div>
                <div class="item">
                  <span class="label">1248sqft</span>
                </div>
              </div>
              <div class="url">
                <span>URL: </span>
                <a href="">https://www.99.co/blog/singapore/a-home-above-allen-and-lorem-ipsum-dolor-sit-amet</a>
              </div>
              <div class="location">
                <div class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                <span>Marriot Hotel, 99 Irrawaddy Road, Novena Singapore, Singapore 329568</span>
              </div>
              <div class="available-viewing-time">
                <p>Available viewing time</p>
                <div class="item">
                  <span>Day</span>
                  <div class="value">Monday</div>
                </div>
                <div class="item">
                  <span>Date</span>
                  <div class="value">July 4 2019</div>
                </div>
                <div class="item">
                  <span>Time</span>
                  <div class="value">11 am - 1 pm</div>
                </div>
              </div>
              <div class="agent">
                <div class="agent-left">
                  <div class="item-avatar">
                    <div class="label">
                      <h2>Jennifer Ford</h2>
                      <p>Propximity Agents</p>
                    </div>
                  </div>
                </div>
                <div class="button">
                  <a href="#" class="btn btn-sm btn-primary">Contact Agent</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="product-details">
            <div class="label">
              <span class="badge badge-success">Tenanted</span>
              <span class="badge badge-warning">2% Comm</span>
            </div>
            <div class="description">
              <div class="title-price">
                <h2>Piermont Grand</h2>
                <div class="price">$799.19</div>
              </div>
              <div class="specification">
                <div class="item">
                  <span class="label">Bedrooms</span>
                  <span class="value">4</span>
                </div>
                <div class="item">
                  <span class="label">Bathrooms</span>
                  <span class="value">2</span>
                </div>
                <div class="item">
                  <span class="label">1248sqft</span>
                </div>
              </div>
              <div class="url">
                <span>URL: </span>
                <a href="">https://www.99.co/blog/singapore/a-home-above-allen-and-lorem-ipsum-dolor-sit-amet</a>
              </div>
              <div class="location">
                <div class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                <span>Marriot Hotel, 99 Irrawaddy Road, Novena Singapore, Singapore 329568</span>
              </div>
              <div class="available-viewing-time">
                <p>Available viewing time</p>
                <div class="item">
                  <span>Day</span>
                  <div class="value">Monday</div>
                </div>
                <div class="item">
                  <span>Date</span>
                  <div class="value">July 4 2019</div>
                </div>
                <div class="item">
                  <span>Time</span>
                  <div class="value">11 am - 1 pm</div>
                </div>
              </div>
              <div class="agent">
                <div class="agent-left">
                  <div class="item-avatar">
                    <div class="label">
                      <h2>Jennifer Ford</h2>
                      <p>Propximity Agents</p>
                    </div>
                  </div>
                </div>
                <div class="button">
                  <a href="#" class="btn btn-sm btn-primary">Contact Agent</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="product-details">
            <div class="label">
              <span class="badge badge-success">Tenanted</span>
              <span class="badge badge-warning">2% Comm</span>
            </div>
            <div class="description">
              <div class="title-price">
                <h2>Piermont Grand</h2>
                <div class="price">$799.19</div>
              </div>
              <div class="specification">
                <div class="item">
                  <span class="label">Bedrooms</span>
                  <span class="value">4</span>
                </div>
                <div class="item">
                  <span class="label">Bathrooms</span>
                  <span class="value">2</span>
                </div>
                <div class="item">
                  <span class="label">1248sqft</span>
                </div>
              </div>
              <div class="url">
                <span>URL: </span>
                <a href="">https://www.99.co/blog/singapore/a-home-above-allen-and-lorem-ipsum-dolor-sit-amet</a>
              </div>
              <div class="location">
                <div class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                <span>Marriot Hotel, 99 Irrawaddy Road, Novena Singapore, Singapore 329568</span>
              </div>
              <div class="available-viewing-time">
                <p>Available viewing time</p>
                <div class="item">
                  <span>Day</span>
                  <div class="value">Monday</div>
                </div>
                <div class="item">
                  <span>Date</span>
                  <div class="value">July 4 2019</div>
                </div>
                <div class="item">
                  <span>Time</span>
                  <div class="value">11 am - 1 pm</div>
                </div>
              </div>
              <div class="agent">
                <div class="agent-left">
                  <div class="item-avatar">
                    <div class="label">
                      <h2>Jennifer Ford</h2>
                      <p>Propximity Agents</p>
                    </div>
                  </div>
                </div>
                <div class="button">
                  <a href="#" class="btn btn-sm btn-primary">Contact Agent</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="product-details">
            <div class="label">
              <span class="badge badge-success">Tenanted</span>
              <span class="badge badge-warning">2% Comm</span>
            </div>
            <div class="description">
              <div class="title-price">
                <h2>Piermont Grand</h2>
                <div class="price">$799.19</div>
              </div>
              <div class="specification">
                <div class="item">
                  <span class="label">Bedrooms</span>
                  <span class="value">4</span>
                </div>
                <div class="item">
                  <span class="label">Bathrooms</span>
                  <span class="value">2</span>
                </div>
                <div class="item">
                  <span class="label">1248sqft</span>
                </div>
              </div>
              <div class="url">
                <span>URL: </span>
                <a href="">https://www.99.co/blog/singapore/a-home-above-allen-and-lorem-ipsum-dolor-sit-amet</a>
              </div>
              <div class="location">
                <div class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                <span>Marriot Hotel, 99 Irrawaddy Road, Novena Singapore, Singapore 329568</span>
              </div>
              <div class="available-viewing-time">
                <p>Available viewing time</p>
                <div class="item">
                  <span>Day</span>
                  <div class="value">Monday</div>
                </div>
                <div class="item">
                  <span>Date</span>
                  <div class="value">July 4 2019</div>
                </div>
                <div class="item">
                  <span>Time</span>
                  <div class="value">11 am - 1 pm</div>
                </div>
              </div>
              <div class="agent">
                <div class="agent-left">
                  <div class="item-avatar">
                    <div class="label">
                      <h2>Jennifer Ford</h2>
                      <p>Propximity Agents</p>
                    </div>
                  </div>
                </div>
                <div class="button">
                  <a href="#" class="btn btn-sm btn-primary">Contact Agent</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="section-listing">
      <div class="top">
        <div class="title">
          <h2>Check out New Launches in the area too!</h2>
          <h5>Contact these agents for personalized viewings</h5>
        </div>
        <div class="view-all"><a href="#">View All</a></div>
      </div>
      <div class="items">
        <div class="item">
          <div class="product-details">
            <div class="label">
              <span class="badge badge-success">Tenanted</span>
              <span class="badge badge-warning">2% Comm</span>
            </div>
            <div class="description">
              <div class="title-price">
                <h2>Piermont Grand</h2>
                <div class="price">$799.19</div>
              </div>
              <div class="specification">
                <div class="item">
                  <span class="label">Bedrooms</span>
                  <span class="value">4</span>
                </div>
                <div class="item">
                  <span class="label">Bathrooms</span>
                  <span class="value">2</span>
                </div>
                <div class="item">
                  <span class="label">1248sqft</span>
                </div>
              </div>
              <div class="url">
                <span>URL: </span>
                <a href="">https://www.99.co/blog/singapore/a-home-above-allen-and-lorem-ipsum-dolor-sit-amet</a>
              </div>
              <div class="location">
                <div class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                <span>Marriot Hotel, 99 Irrawaddy Road, Novena Singapore, Singapore 329568</span>
              </div>
              <div class="available-viewing-time">
                <p>Available viewing time</p>
                <div class="item">
                  <span>Day</span>
                  <div class="value">Monday</div>
                </div>
                <div class="item">
                  <span>Date</span>
                  <div class="value">July 4 2019</div>
                </div>
                <div class="item">
                  <span>Time</span>
                  <div class="value">11 am - 1 pm</div>
                </div>
              </div>
              <div class="agent">
                <div class="agent-left">
                  <div class="item-avatar">
                    <div class="label">
                      <h2>Jennifer Ford</h2>
                      <p>Propximity Agents</p>
                    </div>
                  </div>
                </div>
                <div class="button">
                  <a href="#" class="btn btn-sm btn-primary">Contact Agent</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="product-details">
            <div class="label">
              <span class="badge badge-success">Tenanted</span>
              <span class="badge badge-warning">2% Comm</span>
            </div>
            <div class="description">
              <div class="title-price">
                <h2>Piermont Grand</h2>
                <div class="price">$799.19</div>
              </div>
              <div class="specification">
                <div class="item">
                  <span class="label">Bedrooms</span>
                  <span class="value">4</span>
                </div>
                <div class="item">
                  <span class="label">Bathrooms</span>
                  <span class="value">2</span>
                </div>
                <div class="item">
                  <span class="label">1248sqft</span>
                </div>
              </div>
              <div class="url">
                <span>URL: </span>
                <a href="">https://www.99.co/blog/singapore/a-home-above-allen-and-lorem-ipsum-dolor-sit-amet</a>
              </div>
              <div class="location">
                <div class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                <span>Marriot Hotel, 99 Irrawaddy Road, Novena Singapore, Singapore 329568</span>
              </div>
              <div class="available-viewing-time">
                <p>Available viewing time</p>
                <div class="item">
                  <span>Day</span>
                  <div class="value">Monday</div>
                </div>
                <div class="item">
                  <span>Date</span>
                  <div class="value">July 4 2019</div>
                </div>
                <div class="item">
                  <span>Time</span>
                  <div class="value">11 am - 1 pm</div>
                </div>
              </div>
              <div class="agent">
                <div class="agent-left">
                  <div class="item-avatar">
                    <div class="label">
                      <h2>Jennifer Ford</h2>
                      <p>Propximity Agents</p>
                    </div>
                  </div>
                </div>
                <div class="button">
                  <a href="#" class="btn btn-sm btn-primary">Contact Agent</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="product-details">
            <div class="label">
              <span class="badge badge-success">Tenanted</span>
              <span class="badge badge-warning">2% Comm</span>
            </div>
            <div class="description">
              <div class="title-price">
                <h2>Piermont Grand</h2>
                <div class="price">$799.19</div>
              </div>
              <div class="specification">
                <div class="item">
                  <span class="label">Bedrooms</span>
                  <span class="value">4</span>
                </div>
                <div class="item">
                  <span class="label">Bathrooms</span>
                  <span class="value">2</span>
                </div>
                <div class="item">
                  <span class="label">1248sqft</span>
                </div>
              </div>
              <div class="url">
                <span>URL: </span>
                <a href="">https://www.99.co/blog/singapore/a-home-above-allen-and-lorem-ipsum-dolor-sit-amet</a>
              </div>
              <div class="location">
                <div class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                <span>Marriot Hotel, 99 Irrawaddy Road, Novena Singapore, Singapore 329568</span>
              </div>
              <div class="available-viewing-time">
                <p>Available viewing time</p>
                <div class="item">
                  <span>Day</span>
                  <div class="value">Monday</div>
                </div>
                <div class="item">
                  <span>Date</span>
                  <div class="value">July 4 2019</div>
                </div>
                <div class="item">
                  <span>Time</span>
                  <div class="value">11 am - 1 pm</div>
                </div>
              </div>
              <div class="agent">
                <div class="agent-left">
                  <div class="item-avatar">
                    <div class="label">
                      <h2>Jennifer Ford</h2>
                      <p>Propximity Agents</p>
                    </div>
                  </div>
                </div>
                <div class="button">
                  <a href="#" class="btn btn-sm btn-primary">Contact Agent</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal modal-tag-agent show"> <!-- add class="show" -->
  <div class="content-modal">
    <div class="title">Send Request</div>
    <div class="close">
      <button>
        <i class="fa fa-close"></i>
      </button>
    </div>
    <div class="content">
      <div class="item">
        <div class="product-details">
          <div class="label">
            <span class="badge badge-success">Tenanted</span>
            <span class="badge badge-warning">2% Comm</span>
          </div>
          <div class="description">
            <div class="title-price">
              <h2>Piermont Grand</h2>
              <div class="price">$799.19</div>
            </div>
            <div class="specification">
              <div class="item">
                <span class="label">Bedrooms</span>
                <span class="value">4</span>
              </div>
              <div class="item">
                <span class="label">Bathrooms</span>
                <span class="value">2</span>
              </div>
              <div class="item">
                <span class="label">1248sqft</span>
              </div>
            </div>
            <div class="url">
              <span>URL: </span>
              <a href="">https://www.99.co/blog/singapore/a-home-above-allen-and-lorem-ipsum-dolor-sit-amet</a>
            </div>
            <div class="location">
              <div class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
              <span>Marriot Hotel, 99 Irrawaddy Road, Novena Singapore, Singapore 329568</span>
            </div>
            <div class="available-viewing-time">
              <p>Available viewing time</p>
              <div class="item">
                <span>Day</span>
                <div class="value">Monday</div>
              </div>
              <div class="item">
                <span>Date</span>
                <div class="value">July 4 2019</div>
              </div>
              <div class="item">
                <span>Time</span>
                <div class="value">11 am - 1 pm</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <form>
        <div class="form-group">
          <label>Full Name</label>
          <input type="text" class="form-control" placeholder="Enter full name">
        </div>
        <div class="form-group">
          <label>Mobile No</label>
          <input type="number" class="form-control" placeholder="Enter mobile number">
        </div>
        <div class="form-group">
          <label>Message</label>
          <textarea class="form-control" placeholder="Enter message"></textarea>
        </div>
        <div class="outer-button">
          <button type="submit" class="btn btn-light">Cancel</button>
          <button type="submit" class="btn btn-primary">Submit Request</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php include "base/footer.php" ?>
